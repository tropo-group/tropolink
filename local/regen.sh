#!/usr/bin/env bash
W="$(cd $(dirname $(readlink -f "$0"))/ && pwd)"
vv() { echo "$@">&2;"$@"; }
dvv() { if [[ -n "${DEBUG-}" ]];then echo "$@">&2;fi;"$@"; }
reset
out2="$( cd "$W/.." && pwd)"
out="$(dirname $out2)/$(basename $out2)_pre"
venv=${venv:-$HOME/tools/cookiecutter/activate}
if [ -e "$venv/bin/activate" ];then . "$venv/bin/activate";fi
set -e
if [[ -n "$1" ]] && [ -e $1 ];then
    COOKIECUTTER="$1"
    shift
fi
# $1 maybe a dir or an argument
u=${COOKIECUTTER-}
if [[ -z "$u" ]];then
    u="$HOME/.cookiecutters/cookiecutter-django"
    if [ ! -e "$u" ];then
        u="https://github.com/corpusops/$(basename $u).git"
    else
        cd "$u"
        git fetch origin
        git pull --rebase
    fi
fi
if [ -e "$out" ];then vv rm -rf "$out";fi
vv cookiecutter --no-input -o "$out" -f "$u" \
    name="tropolink" \
    py_ver="3.8" \
    nginx_in_dev="y" \
    ssl_in_dev="y" \
    django_ver="2.2.4" \
    django_ver_1="2.2" \
    git_ns="inra-tropolink" \
    lname="tropolink" \
    out_dir="." \
    tld_domain="makina-corpus.net" \
    app_type="django" \
    use_submodule_for_deploy_code="y" \
    dev_domain="dev-tropolink.makina-corpus.net" \
    qa_domain="qa-tropolink.makina-corpus.net" \
    staging_domain="staging-tropolink.makina-corpus.net" \
    prod_domain="tropolink.makina-corpus.net" \
    dev_alternate_domains="www.dev-tropolink.makina-corpus.net" \
    qa_alternate_domains="www.qa-tropolink.makina-corpus.net" \
    staging_alternate_domains="www.staging-tropolink.makina-corpus.net" \
    prod_alternate_domains="www.tropolink.makina-corpus.net" \
    staging_host="" \
    qa_host="" \
    dev_host="dev-docker-tropolink.makina-corpus.net" \
    prod_host="" \
    staging_branch="staging" \
    dev_branch="dev" \
    prod_branch="prod" \
    qa_branch="qa" \
    staging_port="22" \
    remove_cron="" \
    enable_cron="" \
    qa_port="22" \
    dev_port="40005" \
    django_project_name="tropolink" \
    prod_port="22" \
    django_settings="tropolink.settings" \
    mail_domain="makina-corpus.net" \
    infra_domain="makina-corpus.net" \
    git_server="gitlab.makina-corpus.net" \
    git_project_server="gitlab.makina-corpus.net" \
    git_scheme="https" \
    git_user="" \
    test_tests="y" \
    test_linting="y" \
    haproxy="" \
    git_url="https://gitlab.makina-corpus.net" \
    git_project="api" \
    git_project_url="https://gitlab.makina-corpus.net/inra-tropolink/tropolink-api" \
    fname_slug="inra-tropolink-tropolink" \
    runner="staging-docker-cicd-1.makina-corpus.net" \
    runner_tag="shared-ci-docker-srv1" \
    deploy_project_url="https://github.com/corpusops/django-deploy-common.git" \
    deploy_project_dir="local/django-deploy-common" \
    with_sentry="y" \
    docker_registry="registry2.makina-corpus.net" \
    registry_is_gitlab_registry="" \
    simple_docker_image="inra/tropolink-api" \
    docker_image="registry2.makina-corpus.net/inra/tropolink-api" \
    db_mode="postgres" \
    generic_db_mode="postgres" \
    tz="Europe/Paris" \
    use_i18n="y" \
    use_l10n="y" \
    registry_user="" \
    registry_password="" \
    statics_uri="/static" \
    media_uri="/media" \
    use_tz="y" \
    language_code="fr-fr" \
    rabbitmq_image="corpusops/rabbitmq:3" \
    with_minio="" \
    with_toolbar="y" \
    with_djextensions="y" \
    with_celery="y" \
    celery_broker="rabbitmq" \
    celery_version=">=4.3.0,<5" \
    celery_results_version=">=1.0.4,<2.0" \
    celery_beat_version=">=1.5.0,<2.0" \
    postgis_image="corpusops/postgis:13-3" \
    postgresql_image="corpusops/postgres:13" \
    postgres_image="corpusops/postgres:13" \
    mysql_image="corpusops/mysql" \
    memcached_image="corpusops/memcached:alpine" \
    redis_image="corpusops/redis:4.0-alpine" \
    nginx_image="corpusops/nginx:1.14-alpine" \
    user_model="" \
    settings_account_default_http_protocol="http" \
    settings_use_x_forwarded_host="y" \
    settings_secure_ssl_redirect="y" \
    git_project_https_url="https://gitlab.makina-corpus.net/inra-tropolink/tropolink-api" \
    settings_csrf_cookie_httponly="y" \
    no_lib="" \
    gunicorn_class="gevent" \
    dbsmartbackup_image="corpusops/dbsmartbackup:postgres-13" \
    cache_system="redis" \
    cache_image="corpusops/redis:4.0-alpine" \
    psycopg_req="==2.8.*" \
    session_engine_base="django.contrib.sessions.backends.db" \
    session_engine_prod="django.contrib.sessions.backends.db" \
    memcached_key_prefix="tropolink" \
    with_apptest="" \
    with_postgist_test="y" \
    with_drf="y" \
    with_ftp="" \
    ftp_root="/ftp" \
    ftp_port="21" \
    ftp_int_port="21" \
    ftp_port_range_start="12300" \
    ftp_port_len="100" \
    ftp_port_range="12300:12400" \
    with_ia_libs="" \
    cache_only_in_prod="" \
    with_bundled_front="" \
    with_reset_stagingprod_jobs="" \
    node_version="10.16.0" \
    node_image="corpusops/node:10.16" \
    with_yarn="" \
    js_artefacts="src/*/static" \
    no_private="" \
    base_image="corpusops/ubuntu-bare:20.04" \
    registry_image="registry:2" \
    compose_image="docker/compose:1.27.4" \
    docker_test_image="busybox" \
    dind_image="docker:dind" \
    gitlabci_envs="dev" \
    gitlabci_image="docker/compose:1.27.4" \
    corpusops_image="corpusops/ubuntu:20.04" \
    deploy_playbook=".ansible/playbooks/inra.yml" \
    with_pil="" \
    with_black="y" \
    db_out_port="5436" \
     "$@"


if  [ ! -e lib ];then
    mkdir lib
    touch lib/.empty
fi



# to finish template loop
# sync the gen in a second folder for intermediate regenerations
dvv rsync -aA \
    $(if [[ -n $DEBUG ]];then echo "-v";fi )\
    --include local/regen.sh \
    --exclude "local/*" --exclude lib \
    $( if [ -e ${out2}/.git ];then echo "--exclude .git";fi; ) \
    "$out/" "${out2}/"

( cd "$out2" && git add -f local/regen.sh || /bin/true)
dvv cp -f "$out/local/regen.sh" "$out2/local"

add_submodules() {
    cd "$out"
    while read submodl;do
        submodu="$(echo $submodl|awk '{print $2}')"
        submodp="$(echo $submodl|awk '{print $1}')"
        cd "$out2"
        if [ ! -e "$submodp" ];then
            git submodule add -f "$submodu" "$submodp"
        fi
        cd "$out"
    done < <(\
        git submodule foreach -q 'echo $path `git config --get remote.origin.url`'
    )
    cd "$W"
}
( add_submodules )
if [[ -z ${NO_RM-} ]];then dvv rm -rf "${out}";fi
echo "Your project is generated in: $out2" >&2
echo "Please note that you can generate with the local/regen.sh file" >&2
# vim:set et sts=4 ts=4 tw=80:
