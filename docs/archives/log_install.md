# log install
- To start, install deploy ssh pubkey in ``/root/.ssh/authorized_keys``
  of each server

## Remote access to your VPS, LXCs, configure your ~/.ssh/config
- edit your /etc/hosts if stack is not yet in DNS

    ```
    x.x.x.x dev-docker-tropolink.makina-corpus.net     dev-tropolink.makina-corpus.net
    x.x.x.x       qa-tropolink.makina-corpus.net
    x.x.x.x  staging-tropolink.makina-corpus.net
    x.x.x.x     tropolink.makina-corpus.net
    ```
- Put this in your ``~/.ssh/config``

    ```sshconfig
    Host dev-docker-tropolink.makina-corpus.net
    User root
    ServerAliveInterval 5
    Port 40002
    Host 
    User root
    ServerAliveInterval 5
    Port 22
    ```

## init local deploy
- see [./readme](./README.md#setupvault)

    ```sh
    .ansible/scripts/download_corpusops.sh
    .ansible/scripts/setup_ansible.sh
    : ; CORPUSOPS_VAULT_PASSWORD='supersecret' .ansible/scripts/setup_vaults.sh
    # to review vars and open a crypted inventory file
    .ansible/scripts/edit_vault.sh .ansible/inventory/group_vars/all/default.yml
    .ansible/scripts/call_ansible.sh -vvv .ansible/playbooks/deploy_key_setup.yml
    ```

## Install base softwares (ssh, base pkgs, editors, etc)
```sh
.ansible/scripts/call_ansible.sh -vvv .ansible/playbooks/bootstrap.yml \
    -t vars,base,tools,firewall,docker  -l "baremetals" \
    -e "{cops_vars_debug: true}"
```

## LXC
- install lxc

    ```sh
    .ansible/scripts/call_ansible.sh -l compute_nodes_lxcs \
        */*/*/corpusops.roles/playbooks/provision/lxc_compute_node/main.yml \
        -e "{cops_vars_debug: true}"
    ```
- make a ubuntu template and snapshot it

    ```sh
    export COPS_ROOT="$(pwd)/local/corpusops.bootstrap"
    r=bionic
    h=compute_nodes_lxcs
    .ansible/scripts/call_ansible.sh -vvvv \
        */*/corpusops.roles/playbooks/provision/lxc_container.yml \
        -e "{lxc_host: $h, lxc_container_name: corpusops${r}, ubuntu_release: ${r}}"
    .ansible/scripts/call_ansible.sh -vvvv \
        */*/*/*/playbooks/provision/lxc_container/snapshot.yml \
        -e "{lxc_host: $h, container: corpusops${r}, image: corpusops${r}tpl}"
    .ansible/scripts/call_ansible.sh -vvvv */*/*/lxc_stop/role.yml \
        -l $h -e "{lxc_container_name: corpusops${r}tpl}"
    ```

- create lxcs from this template

    ```sh
    export COPS_ROOT="$(pwd)/local/corpusops.bootstrap"
    h="dev_vps"
    vms="dev-docker-tropolink.makina-corpus.net   "
    set -e
    for i in $vms;do
      .ansible/scripts/call_ansible.sh -vvvv \
        $COPS_ROOT/roles/corpusops.roles/playbooks/provision/lxc_container.yml \
        -e "{lxc_host: $h, lxc_container_name: $i}"
    done
    set +e
    ```

    ```sh
    export COPS_ROOT="$(pwd)/local/corpusops.bootstrap"
    h="prod_vps"
    vms=""
    set -e
    for i in $vms;do
      .ansible/scripts/call_ansible.sh -vvvv \
        $COPS_ROOT/roles/corpusops.roles/playbooks/provision/lxc_container.yml \
        -e "{lxc_host: $h, lxc_container_name: $i}"
    done
    set +e
    ```

## SSL / Letsencrypt

## Install docker everywhere
```sh
.ansible/scripts/call_ansible.sh -vvvv \
    local/*/*/corpusops.roles/services_virt_docker/role.yml
```

## Install gitlab runners
- Install runner package and service, + docker inside ci runners

    ```sh
    .ansible/scripts/call_ansible.sh -vvvv -l ci_runners \
      local/*/*/corpusops.roles/services_ci_gitlab_runner/role.yml
    ```
### Register to gitlab server each CI node runner
- ``pre_clone_script`` is suport important to workaround [gitlab#1736](https://gitlab.com/gitlab-org/gitlab-runner/issues/1736)
- you also need to adjust ``volumes``
- On your docker executor gitlabCI runner, ensure it is configured as the following

    ```sh
    lxc-attach -n staging-docker-cicd-1.makina-corpus.net
    gitlab-runner register
    # - https://gitlab.com/
    # - ``<token>`` on https://gitlab.makina-corpus.net/inra-tropolink/tropolink-api/settings/ci_cd
    # - tags: ["inra-tropolink-tropolink-ci"]
    # - docker
    # - corpusops/ubuntu:18.04
    vim /etc/gitlab-runner/config.toml
    # [[runners]]
    # builds_dir = "/srv/nobackup/gitlabrunner/builds"
    # cache_dir = "/cache"
    # pre_clone@sa_script = "umask 0022"
    # [runners.docker]
    # privileged: true
    # disable_cache: false
    # volumes = ["/cache:/cache", "/srv/nobackup/gitlabrunner/builds:/srv/nobackup/gitlabrunner/builds", "/run/docker.sock:/host-docker-socket/docker.sock"]
    mkdir /srv/nobackup/gitlabrunner/builds /cache -p
    service gitlab-runner restart
    ```

- On https://gitlab.makina-corpus.net/inra-tropolink/tropolink-api/settings/ci_cd / variable
    - setup ``CORPUSOPS_VAULT_PASSWORD``

## Reconfigure haproxy/msiptables (load balancer & firewall)
- do

    ```sh
    export A_ENV_NAME=staging
    # or export A_ENV_NAME=prod
    .ansible/scripts/call_ansible.sh -vvv -l baremetals_${A_ENV_NAME} \
        local/*/*/*/playbooks/provision/lxc_compute_node/main.yml \
        -t lxc_haproxy_registrations,lxc_ms_iptables_registrations
    ```
## hand delivery on dev
- do

    ```sh
    # docker image tag to pull
    export CI_COMMIT_TAG_NAME=v2.1
    export CI_COMMIT_REF_NAME=master
    # staging or  prod
    export A_ENV_NAME=staging
    # or export A_ENV_NAME=prod
    .ansible/scripts/call_ansible.sh -vvv -l $A_ENV_NAME .ansible/playbooks/app.yml
    ```

## Reconfigure letsencrypt
Reconfigure free HTTPS certicates using lets encrypt
```sh
.ansible/scripts/call_ansible.sh -vvvvv \
    -l staging_baremetal,prod_baremetal \
    local/c*/roles/*roles/localsettings_certbot/role.yml \
    .ansible/playbooks/finish_https.yml
```
