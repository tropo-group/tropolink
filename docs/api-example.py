import io
import tarfile
from pprint import pp

from keycloak import KeycloakOpenID
import requests
import time

USER = ''
PASSWORD = ''
CLIENT_ID = ''
CLIENT_SECRET = ''
API_HOST = ''


def refresh_token_if_needed(token):
    """ À appeler avant chaque requête pour être sur d'avoir un access_token à jour """
    token_info = keycloak_openid.introspect(token['access_token'])
    if token_info['exp'] < time.time():
        token = keycloak_openid.refresh_token(token['refresh_token'])
    return token


def get_studies(token):
    """ Exemple de GET """
    response = requests.get(f'{API_HOST}/studies/', headers={
        'Authorization': f"Bearer {token['access_token']}",
        "Accept": "application/json",
        "Content-Type": "application/json",
    })
    response.raise_for_status()
    return response.json()


def create_trajectory(token):
    response = requests.post(
        f'{API_HOST}/trajectories/',
        json={
            "name": f"Test API {time.time()}",
            "description": "",
            "hour_utc": 12,
            "runtime": -48,
            "vertical_motion": 0,
            "top_of_model": 10000,
            "dates": [
                "2008-01-11",
                "2008-01-12",
                "2013-05-26",
                "2013-05-27",
                "2014-11-05",
                "2014-11-06",
                "2015-05-02",
                "2015-05-07",
                "2015-05-15",
                "2015-05-16"
            ],
            "locations": [
                "1 dot1 44.4104967748132 5.98612657276837 1000",
                "2 dot2 43.4393537536011 6.80075807487013 1000",
                "3 dot3 43.4821017141423 6.86543689859377 1000",
                "4 dot4 43.5270329558487 6.86846225926004 1000",
                "5 dot5 44.5645788676285 5.17532152412447 1000",
            ]
        },
        headers={
            'Authorization': f"Bearer {token['access_token']}",
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
    )
    response.raise_for_status()
    return response.json()


def is_trajectory_completed(token, trajectory):
    response = requests.get(f'{API_HOST}/trajectories/{trajectory["id"]}/', headers={
        'Authorization': f"Bearer {token['access_token']}",
        "Accept": "application/json",
        "Content-Type": "application/json",
    })
    response.raise_for_status()
    data = response.json()
    return data['status'] == 'succeeded'


def get_tdumps_archive(token, trajectory):
    response_url = requests.get(f'{API_HOST}/trajectories/{trajectory["id"]}/get_tdumps/', headers={
        'Authorization': f"Bearer {token['access_token']}",
        "Accept": "application/json",
        "Content-Type": "application/json",
    })
    response_url.raise_for_status()
    print("Getting tdumps archive...")
    response_archive = requests.get(f"{API_HOST}/{response_url.json()['url']}")
    response_archive.raise_for_status()
    return tarfile.open(fileobj=io.BytesIO(response_archive.content), mode='r:gz')


def create_connectivity(token, trajectory):
    response = requests.post(
        f'{API_HOST}/connectivities/',
        json={
            "study": trajectory['study'],
            "method": "buffer",
            "dates": [
                "2008-01-11",
                "2008-01-12"
            ],
            "locations": [
                "1 dot1 44.4104967748132 5.98612657276837 1000",
                "2 dot2 43.4393537536011 6.80075807487013 1000",
                "3 dot3 43.4821017141423 6.86543689859377 1000"
            ],
            "parameters": {
                "config": {
                    "method": "buffer",
                    "radius": 10
                },
                # Je ne suis pas sur que cette partie soit utilisé,
                # si c'est le cas c'est lors du calcul par le script R sur le cluster
                "trajectories_studies": [
                    {
                        "label": trajectory['name'],
                        "value": trajectory['study']
                    }
                ]
            },
        },
        headers={
            'Authorization': f"Bearer {token['access_token']}",
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
    )
    response.raise_for_status()
    return response.json()


def is_connectivity_completed(token, connectivity):
    response = requests.get(f'{API_HOST}/connectivities/{connectivity["id"]}/', headers={
        'Authorization': f"Bearer {token['access_token']}",
        "Accept": "application/json",
        "Content-Type": "application/json",
    })
    response.raise_for_status()
    data = response.json()
    return data['status'] == 'succeeded'


def get_connectivity_data(token, connectivity):
    """ Exemple de GET """
    response = requests.get(
        f'{API_HOST}/connectivities/{connectivity["id"]}/get_connectivity_json/',
        headers={
            'Authorization': f"Bearer {token['access_token']}",
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
    )
    response.raise_for_status()
    return response.json()


# Initialisation et connexion à Keycloak
keycloak_openid = KeycloakOpenID(
    server_url="https://staging-tropolink.fr/auth/",
    client_id=CLIENT_ID,
    realm_name="tropolink",
    client_secret_key=CLIENT_SECRET,
)
token = keycloak_openid.token(USER, PASSWORD)

# Création et calcul de la trajectoire
trajectory = create_trajectory(token)
pp(trajectory)

while not is_trajectory_completed(token, trajectory):
    print("Trajectory computing...")
    time.sleep(5)
    token = refresh_token_if_needed(token)

# Récupération des tdumps
token = refresh_token_if_needed(token)
tdumps_archive = get_tdumps_archive(token, trajectory)
pp(tdumps_archive.getnames())

# Création et calcul d'une connectivité
token = refresh_token_if_needed(token)
connectivity = create_connectivity(token, trajectory)
pp(connectivity)

while not is_connectivity_completed(token, connectivity):
    print("Connectivity computing...")
    time.sleep(5)
    token = refresh_token_if_needed(token)

# Récupération du résultat de la connectivité
connectivity_data = get_connectivity_data(token, connectivity)
pp(connectivity_data)
