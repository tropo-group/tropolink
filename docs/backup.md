# plan de sauvegarde

## backend
- `/var/lib/docker` ou si c'est trop gros uniquement `/var/lib/docker/volumes`
    - `/var/lib/docker` indique le docdir de docker.
- `/etc` (en particulier le service systemd dans `/etc/systemd/system`)
- `/home/tropolink` (soit le repertoire où est installé le dépot qui fait tourner l'application)
- Un dump quotidien de la base postgres est intégré a la stack compose et dans le volume `backupdb-dumps`

## reverse proxy
- `/etc` (en particulier `/etc/haproxy`)
- `/home/certbot`

## frontend
- garder le dernier tarball livrable de coté
- avoir un gitlab qui permet de deploy sur netlify

## jupyter
- TODO

