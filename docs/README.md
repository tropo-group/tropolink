# Architecture du projet TROPOLINK

```

                     +----------------------------------------+
                     |      docker/compose backend            |
                     | ┌─────────────┐           ┌────────┐   |
                     | │  Postgresql ◄───────────┤ Celery │   |
                     | └──────▲──────┘           └────┬───┘   |
                     |        │     Nouvelle tache    │       |
                     |    ┌───┴────┐       │     ┌────▼─────┐ |
                   ┌─|────► Django ├───────┴─────► RabbitMQ │ |
                   │ |    └───┬──┬┬┘             └──────────┘ |
                   │ |        │  │└─────────────────────────────────────────────────────────┐
                   │ |        │  └─────────────────────┐      |                             │
                   │ |        │                        │      |                             │
                   │ +------- │ ---------------------- │ -----+                             │
                   │          │                        │                                    │ authentification
                   │          │                        │                                    │      OIDC
                   │          │                        │                                    │
                   │          │                        +─────+                              │
                   │          │                              |                       +------│-------------------------+
                   │          │                              |                       |      │       docker-compose    |
                   │  ┌───────┴───────┐                 ┌────┴────┐ authentification | ┌────▼─────┐    ┌────────────┐ |
Requete sur /api   │  │ Front (React) ├─────────────────┤ Jupyter ├────────────────────► Keycloak │────► PostgreSQL │ |
                   │  └───────▲───────┘                 └────▲────┘      OIDC        | └────▲─────┘    └────────────┘ |
                   │          │                              │                       |      │                         |
                   │          │ Requetes sur /               │                       +------│-------------------------+
                   │          │                              │ Requêtes sur /jupyter        │ Requêtes sur /auth
                   │  ┌───────┴───────┐                      │                              │
                   └──┤ Reverse proxy │──────────────────────┴──────────────────────────────┘
                      └───────▲───────┘
                              │
                              │
                              │
                          Utilisateur

```

## Descriptions des différentes parties
- L'application est pour l'instant formée de 5 "meta-tiers":
    - une application front React
    - une application backend Django
    - un serveur JupyterHub
    - un serveur d'authentification OpenID Connect (Keycloak)
    - un reverse-proxy qui lie ces éléments (HAProxy)
___

### Reverse proxy / Loadbalancer HTTP frontal
- Reçoit toutes les requêtes HTTP/HTTPS et les rediriges aux bons endroits.
- [Documentation de déploiement staging](./haproxy-staging.md)
- doit renvoyer les requêtes de `/api` vers le Django.
- `/` devra contenir les fichiers compilé pour le front (React).

---

### Application Backend
- [Documentation de déploiement](./tropolink_django.md) avec docker-compose.

#### Reverse proxy backend
- reverse proxy nginx

#### Django
- https://docs.djangoproject.com/en/3.1/howto/deployment/
- Application principale contenant le code métier ainsi que la gestion des utilisateurs.
- Elle permet notamment de communiquer avec le cluster de calculs pour les calculs de trajectoires, et de calculer les connectivités.

#### Postgresql
- Base de données de l'application.
- Elle stocke le contenu des études créées et les paramètres des calculs lancés par chaque utilisateur.
- Une étude est définie comme étant :
   - 1 calcul de trajectoires
   - 0 ou N calculs de connectivités


#### Celery
- Moteur de taches asynchrone qui exécute les tâches grâce aux workers. Permet aussi de créer de nouvelles tâches périodiquement grâce à Celery Beat.
- https://docs.celeryproject.org/en/latest/userguide/daemonizing.html
- Ne pas oublier le beat (il faut un beat et autant de worker que nécessaire).

#### RabbitMQ
- Stocke les tâches pour celery.
- Les tâches sont soit le contrôle des statuts des calculs de l'utilisateur, soit le calcul de connectivités.

---

### Application Front (React)
- L'interface web que les utilisateurs finaux utilisent pour configurer et suivre leurs calculs.
- Il faut installer la version de node précisé dans le fichier `.nvmrc`.
- Il faut installer les dépendances avec `npm install`.
- Les fichiers sont compilé avec la commande `npm run build`.
- Les fichiers statiques générés sont servis par un Nginx derrière HAProxy.
- [Documentation de déploiement sur la staging](./front-staging.md)

---

### Serveur JupyterHub
- Un compte unique (`tropolink3`) est utilisé par tous les utilisateurs de la plateforme Tropolink.
- Un noyau spécifique est utilisé, basé sur un environnement conda qui contient toutes les dépendances nécessaires.
- Un notebbok permet d'afficher le graphe résultat d'un calcul de connectivités, et d'accéder à la matrice de connectivités.
- Le notebook appele le package Python `nbconnectivity` créé pour le projet.
- [Documentation de déploiement](jupyterhub.md)

### Keycloak
- Permet d'unifier l'authentification entre Django et JupyterHub en utilisant le protocole OpenID Connect
- [Documentation de déploiement](keycloak.md)

---
## Plan de sauvegarde

- [cf backup](./backup.md)

