# Mise en place d'un HAProxy en front pour répartition des requêtes

Le serveur de staging (staging-tropolink.fr) héberge une instance de JupyterHub et l'API Tropolink. Le même domaine staging-tropolink.fr est utilisé pour accéder à JupyterHub, l'API Tropolink et le front Tropolink :

- front Tropolink : https://staging-tropolink.fr/
- API Tropolink : https://staging-tropolink.fr/api
- JupyterHub : https://staging-tropolink.fr/jupyter

Cette page couvre la procédure d'installation du HAProxy, avec certificat Let's Encrypt.

## HAProxy

- Installer HAProxy :
  ```sh
  apt install haproxy
  ```
- ajouter sa conf :
  ```sh
  cat <<EOT >/etc/haproxy/haproxy.cfg
  global
      log /dev/log    local0
      log /dev/log    local1 notice
      chroot /var/lib/haproxy
      stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
      stats timeout 30s
      user haproxy
      group haproxy
      daemon

      # Default SSL material locations
      ca-base /etc/ssl/certs
      crt-base /etc/ssl/private

      # Default ciphers to use on SSL-enabled listening sockets.
      # For more information, see ciphers(1SSL). This list is from:
      #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
      # An alternative list with additional directives can be obtained from
      #  https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=haproxy
      ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
      ssl-default-bind-options no-sslv3 no-tls-tickets
      tune.ssl.default-dh-param 2048

  defaults
      log global
      mode http
      option httplog
      option dontlognull

      timeout connect 3s
      timeout client  300s
      timeout server  300s

      errorfile 400 /etc/haproxy/errors/400.http
      errorfile 403 /etc/haproxy/errors/403.http
      errorfile 408 /etc/haproxy/errors/408.http
      errorfile 500 /etc/haproxy/errors/500.http
      errorfile 502 /etc/haproxy/errors/502.http
      errorfile 503 /etc/haproxy/errors/503.http
      errorfile 504 /etc/haproxy/errors/504.http

      default-server maxconn 250 on-error fail-check

  # Frontends
  #

  frontend default-http
      bind :80
      ##bind :443 ssl crt /etc/ssl/haproxy/ strict-sni no-tlsv11 no-tlsv10 no-sslv3

      option forwardfor
      http-request set-header X-Forwarded-Proto https if { ssl_fc }

      capture request header Host len 64

      # Force SSL
      ##http-request redirect scheme https code 301 if !{ ssl_fc }
      ##http-response set-header Strict-Transport-Security "max-age=16000000; preload;"

      http-request redirect scheme https code 301 drop-query append-slash if { path -i /jupyter }

      acl acmechallenge path_beg /.well-known/acme-challenge/
      use_backend backend_acmechallenge if acmechallenge

      acl uri_jupyter path_beg -i /jupyter
      acl uri_api path_beg -i /api
      acl uri_keycloak path_beg -i /auth

      use_backend backend_jupyter if uri_jupyter
      use_backend backend_api if uri_api
      use_backend backend_keycloak if uri_keycloak
      default_backend backend_front

  # Backends
  #

  backend backend_acmechallenge
      server certbot 127.0.0.1:6000

  backend backend_jupyter
      server jupyter 127.0.0.1:8090

  backend backend_api
      server api 127.0.0.1:8080

  backend backend_front
      server front 127.0.0.1:8010

  backend backend_keycloak
      server keycloak 127.0.0.1:8081
  EOT
  ```
- Recharger sa config :
  ```sh
  systemctl reload haproxy
  ```

## JupyterHub

- changer le port par défaut de traefik et la base URI de Jupyterhub :
  ```sh
  echo "c.JupyterHub.base_url = '/jupyter'" >/opt/tljh/config/jupyterhub_config.d/baseurl.py
  /opt/tljh/hub/bin/tljh-config set http.port 8090
  /opt/tljh/hub/bin/tljh-config reload proxy
  ```

## certbot / certificat Let's Encrypt

- Installer certbot
  ```sh
  apt install certbot
  ```
- ajouter le hook pour HAProxy
  ```sh
  mkdir -p /etc/letsencrypt/renewal-hooks/deploy/
  cat <<EOT >/etc/letsencrypt/renewal-hooks/deploy/90-haproxy.sh
  #!/bin/sh
  set -eu
  umask 077

  cat ${RENEWED_LINEAGE}/fullchain.pem ${RENEWED_LINEAGE}/privkey.pem /etc/ssl/dhparam-2048.pem >/etc/ssl/haproxy/$(basename ${RENEWED_LINEAGE}).pem

  /bin/systemctl reload haproxy.service
  EOT
  chmod +x /etc/letsencrypt/renewal-hooks/deploy/90-haproxy.sh
  ```
- obtenir le certificat
  ```sh
  export DOMAIN=staging-tropolink.fr
  openssl dhparam -out /etc/ssl/dhparam-2048.pem 2048
  certbot certonly --non-interactive --domain $DOMAIN \
      --standalone --http-01-port 6000 --http-01-address 127.0.0.1 \
      --agree-tos --email rde@makina-corpus.com --no-eff-email
  mkdir /etc/ssl/haproxy/
  chmod 700 /etc/ssl/haproxy/
  RENEWED_LINEAGE=/etc/letsencrypt/live/$DOMAIN /etc/letsencrypt/renewal-hooks/deploy/90-haproxy.sh
  ```
- si l'obtention du certificat a réussi, décommenter la ligne `bind :443 ssl crt /etc/ssl/haproxy/ strict-sni` ainsi que la redirection HTTP → HTTPS dans la config de HAProxy :
  ```sh
  sed -i 's/##bind :443 /bind :443 /' /etc/haproxy/haproxy.cfg
  sed -i '/# Force SSL/,+2{s/##//}' /etc/haproxy/haproxy.cfg
  systemctl reload haproxy
  ```
