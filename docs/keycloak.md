# Installation de Keycloak

- Récupérer le dépôt git de la stack docker-compose :
  ```
  $ git clone git@gitlab.makina-corpus.net:inra-tropolink/keycloak.git
  $ cd keycloak/
  ```
- mettre en place la config des différents conteneurs :
  ```
  $ mv .env.dist .env
  $ mv postgresql.env.dist postgresql.env  # And set PostgreSQL password
  $ mv keycloak.env.dist keycloak.env  # And set PostgreSQL and Keycloak admin password
  ```
- lancer la stack :
  ```
  $ docker-compose up -d
  ```

- mettre en place le service systemd pour gérer la stack :
  ```
  # cat <<EOT >/etc/systemd/system/keycloak.service
  [Unit]
  Description=Keycloak docker-compose service
  Before=
  After=docker.service network.service
  Requires=docker.service
  
  [Service]
  Environment="PATH=/bin:/usr/sbin:/usr/bin:/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin:/snap/bin"
  Restart=on-failure
  RestartSec=0
  TimeoutSec=300
  WorkingDirectory=/home/tropolink3/keycloak
  ExecStartPre=/usr/bin/env docker-compose -f "docker-compose.yml" config
  ExecStart=/usr/bin/env    docker-compose -f "docker-compose.yml" up
  ExecStop=/usr/bin/env     docker-compose -f "docker-compose.yml" config
  ExecStop=/usr/bin/env docker-compose -f "docker-compose.yml" down
  
  [Install]
  WantedBy=multi-user.target
  EOT
  ```
  ```
  # systemctl daemon-reload
  # systemctl enable keycloak.service
  # systemctl start keycloak.service
  ```
- Keycloak devrait répondre sur https://staging-tropolink.fr/auth, le compte et le mot de passe définit précédemment dans la configuration du conteneur permet de s'y connecter.
- créer un nouveau realm et importer le fichier `realm.json` de ce dépôt.
