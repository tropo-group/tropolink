# Configuration de Nginx pour servir la partie front

Nginx écoute en local sur le port 8010/tcp et reçoit de HAProxy les requêtes pour le front.
Son *document root* est dans `/home/tropolink3/tropolink-front/dist/`.

## Installation

```
# apt install nginx
```

## Configuration

- Désactivation du vhost par défaut :
  ``` shell
  # rm /etc/nginx/sites-enabled/default
  ```
- ajout du vhost pour tropolink-front :
  ```shell
  # cat <<EOT >/etc/nginx/sites-available/tropolink.conf
  server {
      listen 127.0.0.1:8010 default_server;
      root /home/tropolink3/tropolink-front/dist/;

      index index.html;
      location / {
    	  try_files $uri $uri/ /index.html =404;
      }
  }
  ````
- activation du vhost et redémarrage :
  ```
  # cd /etc/nginx/sites-enabled/ && ln -s ../sites-available/tropolink.conf
  # systemctl restart nginx
  ```

## Déploiement et mise à jour

À faire en tant qu'utilisateur _tropolink3_ :

- Initialisation du dossier de code *(uniquement la première fois)* :
  ```shell
  tropolink3$ git clone https://gitlab.makina-corpus.net/inra-tropolink/tropolink.git ~/tropolink-front/
  ```
- Récupération de la dernière version *(à chaque fois)* :
  ```shell
  tropolink3$ cd ~/tropolink-front/
  tropolink3$ git pull
  ```
- Génération du site statique *(via un conteneur temporaire)* :
  ```shell
  tropolink3$ ./build.sh
  ```
- éditer les URL du back, de Keycloak et de JupyterHub dans le `.env` si besoin (celles commitées sont pour la staging)
- s'assurer que les permissions sont correctes :
  ```shell
  tropolink3$ chmod -R go=rX dist/
  ```
