# Installation de JupyterHub

La version tljh (The littlest JupyterHub) de JupyterHub est installé suivant la [doc officielle](https://tljh.jupyter.org/en/latest/install/custom-server.html).

À l'installation, le démarrage de traefic (le proxy fourni avec JupyterHub) échouera car il cherche à écouter sur le port 80/tcp déjà utilisé par HAProxy. Changer le port par défaut de traefik et la base URI de Jupyterhub :
```
# echo "c.JupyterHub.base_url = '/jupyter'" >/opt/tljh/config/jupyterhub_config.d/baseurl.py
# /opt/tljh/hub/bin/tljh-config set http.port 8090
# cat <<EOT >/opt/tljh/config/traefik_config.d/port.toml
[entryPoints.http]
address = "127.0.0.1:8090"
EOT
# /opt/tljh/hub/bin/tljh-config reload proxy
```
Puis relancer le script d'installation (il échouera à nouveau à la vérification mais cela peut être ignoré, à partir de là JupyterHub est accessible).

## Authentification sur Keycloak

```
# cat <<EOT >/opt/tljh/config/jupyterhub_config.d/oauthenticator.py
c.Authenticator.auto_login = True

c.GenericOAuthenticator.client_id = "jupyterhub"
c.GenericOAuthenticator.client_secret = "<à récupérer dans Keycloak>"
c.GenericOAuthenticator.oauth_callback_url = "https://staging-tropolink.fr/jupyter/hub/oauth_callback"
c.GenericOAuthenticator.token_url = "https://staging-tropolink.fr/auth/realms/tropolink/protocol/openid-connect/token"
c.GenericOAuthenticator.userdata_url = "https://staging-tropolink.fr/auth/realms/tropolink/protocol/openid-connect/userinfo"
c.GenericOAuthenticator.userdata_method = "GET"
c.GenericOAuthenticator.username_key = "preferred_username"

# Handle Keycloak logout
# From https://github.com/jupyterhub/oauthenticator/issues/107#issuecomment-690249793.

from traitlets import Unicode
from oauthenticator.generic import GenericOAuthenticator
from jupyterhub.utils import url_path_join
from jupyterhub.handlers.login import LogoutHandler
from tornado.httputil import url_concat

class KeycloakLogoutHandler(LogoutHandler):
    """Logout handler for keycloak"""

    async def render_logout_page(self):
        params = {
            'redirect_uri': '%s://%s%s' % (
                self.request.protocol,
                self.request.host,
                self.hub.server.base_url),
            }
        self.redirect(
            url_concat(self.authenticator.keycloak_logout_url, params),
            permanent=False
        )

class KeycloakAuthenticator(GenericOAuthenticator):
    """Authenticator handling keycloak logout"""

    keycloak_logout_url = Unicode(
        config=True,
        help="The keycloak logout URL"
    )

    def get_handlers(self, app):
        return super().get_handlers(app) + [(r'/logout', KeycloakLogoutHandler)]

c.JupyterHub.authenticator_class = KeycloakAuthenticator
c.KeycloakAuthenticator.keycloak_logout_url = "https://staging-tropolink.fr/auth/realms/tropolink/protocol/openid-connect/logout"
```
Il faut aussi éditer le service systemd pour y ajouter des variables d'environnements :
```
# systemctl edit jupyterhub.service
[Service]
Environment=OAUTH2_AUTHORIZE_URL=https://staging-tropolink.fr/auth/realms/tropolink/protocol/openid-connect/auth
Environment=OAUTH2_TOKEN_URL=https://staging-tropolink.fr/auth/realms/tropolink/protocol/openid-connect/token
Environment=OAUTH_CALLBACK_URL=https://staging-tropolink.fr/jupyter/hub/oauth_callback
# systemctl daemon-reload
```

Puis redémarrer jupyterhub :
```
# systemctl restart jupyterhub
```

## Notebook

Cloner le dépôt git dans le home de `tropolink3`. Il sera accédé depuis les comptes JupyterHub par un lien symbolique.
```
tropolink3$ git clone https://forgemia.inra.fr/tropo-group/notebook.git
```
Puis configurer les URL de l'API dans `/home/tropolink3/notebook/nbconnectivity/settings.py`.

Créer le lien symbolique et configurer le `.bashrc` de template :
```
# cd /etc/skel/
# ln -s /home/tropolink3/notebook/ notebook
# cat <<EOT >>/etc/skel/.bashrc

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/tljh/user/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/tljh/user/etc/profile.d/conda.sh" ]; then
        . "/opt/tljh/user/etc/profile.d/conda.sh"
    else
        export PATH="/opt/tljh/user/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
export PATH="$PATH:/opt/tljh/user/bin/"
EOT
```

Création de l'environnement Conda et du kernel Jupyter, à partir du `tropolink_env` du dépôt git :
```
# /opt/tljh/user/bin/conda env create --prefix /opt/tljh/user/envs/tropolink_env/ --file tropolink_env.yml
# mkdir /opt/tljh/user/share/jupyter/kernels/tropolink_env/
# cat <<EOT >/opt/tljh/user/share/jupyter/kernels/tropolink_env/kernel.json
{
 "argv": [
  "/opt/tljh/user/envs/tropolink_env/bin/python",
  "-m",
  "ipykernel_launcher",
  "-f",
  "{connection_file}"
 ],
 "display_name": "tropolink_env",
 "language": "python",
 "env": {"R_HOME":"/opt/tljh/user/envs/tropolink_env/lib/R"}
}
EOT
```

Activation des extensions Jupyter requises :
```
# /opt/tljh/user/bin/pip install jupyter_contrib_nbextensions jupyter-notebookparams
# /opt/tljh/user/bin/jupyter contrib nbextension install --sys-prefix
# /opt/tljh/user/bin/jupyter nbextension enable init_cell/main --sys-prefix
# /opt/tljh/user/bin/jupyter nbextension enable hide_input/main --sys-prefix
# /opt/tljh/user/bin/jupyter nbextension install --py jupyter_notebookparams --sys-prefix
# /opt/tljh/user/bin/jupyter nbextension enable --py jupyter_notebookparams --sys-prefix
```

Mise à jour de l'environnement Conda et du kernel Jupyter :

```
# sudo /opt/tljh/user/bin/conda env update -n tropolink_env --file tropolink_env.yml
```
