# Specific doc to deploy on tropolink envs

## Generic instructions

- We assume the stack will be installed inside ``/home/tropolink3/tropolinkapi/``, adapt the clone command and systemd unit if you change this path;
- Unless specified, the following commands must be run as _tropolink3_ user.

### note on inra-staging
- We have first a bastion host `inra-proxy`, we must connect to next hosts via this ssh proxy.
- Then we have the django app running on `staging-tropolink.fr`, we connect to it with tropolink3 (suoder)
- This app connects through a cluster through a SSH tunnel via `biosp-cluster-frontal`, we connect to it with tropolink3 (NON sudoer)

```
SSH >  INRA-proxy --- staging-tropolink.fr
             |              |
             +------  biosp-cluster-frontal
```

### Requirements (MANDATORY)
- Decent OS, either:
    - Latest ubuntu LTS
    - Latest Debian
    - Latest CENTOS
- docker: latest version
- docker-compose: latest version
    - follow official procedures for [docker](https://docs.docker.com/install/#releases)
      and  [docker-compose](https://docs.docker.com/compose/install/).


### Get the code
```sh
git clone https://gitlab.makina-corpus.net/inra-tropolink/tropolink-api.git /home/tropolink3/tropolinkapi
```

### Update code & submodules
Never forget to grab and update regulary the project submodules:

```sh
cd /home/tropolink3/tropolinkapi
git pull
git submodule init # only the fist time
git submodule update --recursive
```

### Configuration

#### SSH

You need to configure a **TROPOLINK_HOST** to connect to:

- Create the file ``./local/.ssh/config`` **in the project repository** with one host that can connect **NON interactively** to tropolink, here it is **biosp-cluster-frontal**:
	- Version without a jump host (BASTION), if the application will run on a Tropolink's server:

		```ssh
        ServerAliveInterval 5
        IdentitiesOnly yes
        UserKnownHostsFile /dev/null
        ConnectTimeout 0
        HashKnownHosts yes
        StrictHostKeyChecking no
        Host biosp-cluster-frontal
            Hostname 147.100.14.120
            User tropolink3
            IdentityFile /code/.ssh/inradeploy
		```
	- Version with two jump hosts (BASTION), to be used on your local machine:

		```ssh
		ServerAliveInterval 5
        IdentitiesOnly yes
        UserKnownHostsFile /dev/null
        ConnectTimeout 0
        HashKnownHosts yes
        StrictHostKeyChecking no
        Host ssh-proxy
            Hostname ssh-proxy.makina-corpus.net
            User ssh-inradeploy
            IdentityFile /code/.ssh/inradeploy
        Host staging-tropolink.fr
            Hostname staging-tropolink.fr
            User tropolink3
            IdentityFile /code/.ssh/inradeploy
            ProxyCommand ssh -W %h:%p ssh-proxy
        Host biosp-cluster-frontal
            Hostname 147.100.14.120
            User tropolink3
            IdentityFile /code/.ssh/inradeploy
            ProxyCommand ssh -W %h:%p staging-tropolink.fr
		```

- then, copy the ssh private key into ``local/.ssh/inradeploy`` (See [Vaultier](https://password.makina-corpus.net/#/workspaces/w/makinacorpus-passwords/vaults/v/credentials-2/cards/c/inra-tropolink/secrets), and fix its permissions:
  ```ssh
  chmod 600 local/.ssh/inradeploy
  ```
- You can then set later in this doc ``TROPOLINK_HOST=biosp-cluster-frontal`` in your ``docker.env``

#### APP
Use the wrapper to init configuration files from their ``.dist`` counterpart
and adapt them to your needs.

```sh
cat .env.dist .env.prod.dist > .env
touch docker.env local.py
```

- This will init, but you can edit later:
	- ``.env`` (from ``.env.prod.dist``)
        At least set and change any password alike to a secured value

    - Set production settings in `.env`

        ```env
        # touch docker.env
        # cat .env
		DJANGO_ENV=production
		DJANGO__DEPLOY_ENV=tropolink
        TROPOLINK_HOST=biosp-cluster-frontal
		DJANGO__HTTP_PROTECT_PASSWORD=secure
        DJANGO_SETTINGS_MODULE=tropolink.settings.instances.prod
		```
	- ``src/tropolink/settings/local.py`` (from ``src/tropolink/settings/local.py.dist``)

        ```sh
        touch src/tropolink/settings/local.py
        ```

### Cook the image
```sh
./control.sh build
```

### Start manually and test, then stop to start with the system
```sh
./control.sh up
docker ps # To see if the container is started
curl https://staging-tropolink.fr/api/ping/  # should return an OK-alike JSON response. Can take time before working
```

#### Create the superuser

```sh
./control.sh dcompose exec django ../venv/bin/python manage.py createsuperuser
```

#### test what you want and when you are done:
```sh
./control.sh down
```

### Wire to system supervisor / reboot

As root, adapt this unit to match your system: ``/etc/systemd/system/tropolinkapi.service`` with the content

```systemd
[Unit]
Description=tropolink DockerCompose service
Before=
After=docker.service network.service
Requires=docker.service
[Service]
Environment="PATH=/bin:/usr/sbin:/usr/bin:/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin:/snap/bin"
Restart=on-failure
RestartSec=0
TimeoutSec=300
WorkingDirectory=/home/tropolink3/tropolinkapi
ExecStartPre=/usr/bin/env docker-compose -f "docker-compose.yml" -f "docker-compose-prod.yml"   config
ExecStartPre=/usr/bin/env docker-compose -f "docker-compose.yml" -f "docker-compose-prod.yml"   config
ExecStart=/usr/bin/env    docker-compose -f "docker-compose.yml" -f "docker-compose-prod.yml"   up
ExecStop=/usr/bin/env     docker-compose -f "docker-compose.yml" -f "docker-compose-prod.yml"   config
ExecStop=/usr/bin/env     docker-compose -f "docker-compose.yml" -f "docker-compose-prod.yml"   stop
ExecStopPost=/usr/bin/env docker-compose -f "docker-compose.yml" -f "docker-compose-prod.yml"   config
ExecStopPost=/usr/bin/env docker-compose -f "docker-compose.yml" -f "docker-compose-prod.yml"   down
[Install]
WantedBy=multi-user.target
# vim: set ft=systemd:
```


#### restart application
Then load, and activate it on next reboot
```sh
systemctl daemon-reload
systemctl enable tropolinkapi
# if already started:
# systemctl stop tropolinkapi
systemctl start tropolinkapi
```

## Deployment to the staging server

The staging server is at `staging-tropolink.fr`.

The stack is installed under `/home/tropolink3/tropolinkapi` directory.

### redeploy/update procedure

```sh
ssh tropolink3@staging-tropolink.fr  # See Vaultier for the password
```

```sh
cd ~/tropolinkapi
git pull  # Use your GitLab credentials
git submodule update --recursive
./control.sh pull
./control.sh build
systemctl stop tropolinkapi
systemctl start tropolinkapi
```
