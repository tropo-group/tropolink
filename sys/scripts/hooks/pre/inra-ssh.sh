#!/usr/bin/env bash
if [[ -n $NO_STARTUP_LOGS ]];then
    VERBOSE=""
else
    VERBOSE="-v"
fi
for a in /code/.ssh /root/.ssh /home/$APP_USER/.ssh;do
    if [ ! -e $a ];then mkdir -p $VERBOSE $a;fi
    for i in /ssh.in/inradeploy* /ssh.in/config;do
        cp -f $VERBOSE  $i $a
    done
done
chown $VERBOSE -Rf root:root /root/.ssh
chown $VERBOSE -Rf $APP_USER:$APP_USER /code/.ssh /home/$APP_USER/.ssh
chmod $VERBOSE g-rwx,o-rwx /code/.ssh /home/$APP_USER/.ssh /root/.ssh
chmod $VERBOSE -f 600 /code/.ssh/*
