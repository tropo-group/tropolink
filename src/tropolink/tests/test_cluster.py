import datetime
import io
import json
import os
from unittest import mock

from connectivities.models import Connectivity
from connectivities.tests.factories import ConnectivityFactory
from django.test import TestCase

from studies.tests.factories import StudyFactory
from trajectories.tests.factories import TrajectoryFactory
from tropolink.cluster import ping_cluster, run_hysplit, update_trajectories_status, update_connectivities_status, \
    run_connectivity_computation, get_tdump_list, rm_study, rm_connectivity, get_connectivity_json, \
    get_connectivity_dataframe, get_tdumps_archive_file, get_trajectory_logs_archive_file, \
    get_connectivity_logs_archive_file, get_dates


class ClusterTest(TestCase):
    def test_get_dates(self):
        with self.subTest("one day"):
            nb_dates, dates = get_dates(datetime.datetime(2020, 2, 5, 0), 12)
            self.assertListEqual(list(dates), [datetime.date(2020, 2, 5)])

        with self.subTest("two day"):
            nb_dates, dates = get_dates(datetime.datetime(2020, 2, 5, 1), 20)
            self.assertListEqual(list(dates), [datetime.date(2020, 2, 5), datetime.date(2020, 2, 6)])

        with self.subTest("future"):
            nb_dates, dates = get_dates(datetime.datetime(2020, 2, 5, 0), 60)
            self.assertListEqual(list(dates), [datetime.date(2020, 2, 5), datetime.date(2020, 2, 6), datetime.date(2020, 2, 7)])

        with self.subTest("past"):
            nb_dates, dates = get_dates(datetime.datetime(2020, 2, 5, 0), -60)
            self.assertListEqual(list(dates), [datetime.date(2020, 2, 2), datetime.date(2020, 2, 3), datetime.date(2020, 2, 4), datetime.date(2020, 2, 5)])

        with self.subTest("past end of day"):
            nb_dates, dates = get_dates(datetime.datetime(2020, 2, 5, 22), -71)
            self.assertListEqual(list(dates), [datetime.date(2020, 2, 2), datetime.date(2020, 2, 3), datetime.date(2020, 2, 4), datetime.date(2020, 2, 5), datetime.date(2020, 2, 6)])

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_ping_cluster(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        stdout = io.BytesIO(b'SSH OK')
        mock_ssh.exec_command.return_value = None, stdout, None

        ret = ping_cluster()
        self.assertEqual(ret, 'SSH OK')

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_run_hysplit(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp
        stdout = io.BytesIO(b'Submitted batch job 30333')
        mock_ssh.exec_command.return_value = None, stdout, None

        study = StudyFactory()

        ret = run_hysplit(study, ['cfile'], ['xfile'], [datetime.date(2022, 1, 1)])
        self.assertDictEqual(ret, {'30333': '2022-01-01'})

    @mock.patch('tropolink.cluster.connect_cluster')
    @mock.patch('trajectories.tasks.after_trajectory_success')
    def test_update_trajectories_status(self, mock_after_trajectory_success, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        outs = [
            # First trajectory
            [None, io.BytesIO(b'12\n22'), None],
            [None, io.BytesIO(b'14'), None],
            [None, io.BytesIO(b'34'), None],

            # Second trajectory
            [None, io.BytesIO(b'101\n102'), None],
            [None, io.BytesIO(b''), None],
            [None, io.BytesIO(b''), None],
        ]
        mock_ssh.exec_command.side_effect = outs

        trajectory_with_failed = TrajectoryFactory(
            jobs={
                '12': '2022-01-01',
                '22': '2022-01-02',
                '14': '2022-01-03',
                '34': '2022-01-04',
            },
        )
        trajectory_success = TrajectoryFactory(
            jobs={
                '101': '2022-01-01',
                '102': '2022-01-02',
            },
            jobs_successful=[101],
            jobs_failed=[],
        )
        # Already completed trajectory
        TrajectoryFactory(
            jobs={
                '1': '2022-01-01',
                '2': '2022-01-02',
            },
            jobs_successful=[1, 2],
        )

        update_trajectories_status()

        trajectory_with_failed.refresh_from_db()
        self.assertEqual(trajectory_with_failed.jobs_successful, [12, 22])
        self.assertEqual(trajectory_with_failed.jobs_failed, [14, 34])
        trajectory_success.refresh_from_db()
        self.assertEqual(trajectory_success.jobs_successful, [101, 102])
        self.assertEqual(trajectory_success.jobs_failed, [])

        mock_after_trajectory_success.delay.assert_any_call(trajectory_with_failed.pk)
        mock_after_trajectory_success.delay.assert_any_call(trajectory_success.pk)

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_update_connectivities_status(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        outs = [
            # First connectivity
            [None, io.BytesIO(b'12'), None],

            # Second connectivity
            [None, io.BytesIO(b''), None],
            [None, io.BytesIO(b''), None],
            [None, io.BytesIO(b'34'), None],

            # Third connectivity
            [None, io.BytesIO(b''), None],
            [None, io.BytesIO(b''), None],
            [None, io.BytesIO(b''), None],
            [None, io.BytesIO(b'85'), None],
        ]
        mock_ssh.exec_command.side_effect = outs

        connectivity_success = ConnectivityFactory(
            progress=12,
            cluster_job=12,
        )
        connectivity_failed = ConnectivityFactory(
            cluster_job=34,
        )
        connectivity_progress = ConnectivityFactory(
            cluster_job=85,
        )
        # Already completed connectivity
        ConnectivityFactory(
            status=Connectivity.Status.SUCCEEDED,
            cluster_job=4,
        )

        update_connectivities_status()

        connectivity_success.refresh_from_db()
        self.assertEqual(connectivity_success.status, Connectivity.Status.SUCCEEDED)
        self.assertIsNone(connectivity_success.progress)
        connectivity_failed.refresh_from_db()
        self.assertEqual(connectivity_failed.status, Connectivity.Status.FAILED)
        self.assertIsNone(connectivity_failed.progress)
        connectivity_progress.refresh_from_db()
        self.assertEqual(connectivity_progress.status, Connectivity.Status.IN_PROGRESS)
        self.assertEqual(connectivity_progress.progress, 85)

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_run_connectivity_computation(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp
        stdout = io.BytesIO(b'Submitted batch job 30333')
        mock_ssh.exec_command.return_value = None, stdout, None

        connectivity = ConnectivityFactory()

        job = run_connectivity_computation(connectivity)
        self.assertEqual(job, '30333')

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_get_tdump_list(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp
        mock_sftp.listdir.return_value = [
            'tdump_123', 'tdump_456', 'other_file', 'tdump_789',
        ]
        mock_sftp.open.return_value.__enter__.side_effect = [
            '1', '2', '3',
        ]

        ret = list(get_tdump_list(TrajectoryFactory()))

        self.assertEqual(ret, [
            ('tdump_123', '1'),
            ('tdump_456', '2'),
            ('tdump_789', '3'),
        ])

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_get_tdumps_archive_file(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp
        mock_file = io.BytesIO(b'foobar')
        mock_sftp.open.return_value.__enter__.return_value = mock_file

        generator = get_tdumps_archive_file(StudyFactory())
        archive = next(generator)
        self.assertEqual(archive.read(), b'foobar')
        generator.close()

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_get_trajectory_logs_archive_file(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp
        mock_file = io.BytesIO(b'foobar')
        mock_sftp.open.return_value.__enter__.return_value = mock_file

        generator = get_trajectory_logs_archive_file(StudyFactory())
        archive = next(generator)
        self.assertEqual(archive.read(), b'foobar')
        generator.close()

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_get_connectivity_logs_archive_file(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp
        mock_file = io.BytesIO(b'foobar')
        mock_sftp.open.return_value.__enter__.return_value = mock_file

        generator = get_connectivity_logs_archive_file(ConnectivityFactory())
        archive = next(generator)
        self.assertEqual(archive.read(), b'foobar')
        generator.close()

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_rm_study(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh

        rm_study(StudyFactory())

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_rm_connectivity(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh

        rm_connectivity(ConnectivityFactory())

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_get_connectivity_json(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp

        with open(os.path.join(os.path.dirname(__file__), 'connectivity.json'), 'rb') as f:
            expected_json = f.read()
            mock_file = io.BytesIO(expected_json)

        mock_sftp.open.return_value.__enter__.return_value = mock_file

        result_json = get_connectivity_json(ConnectivityFactory())

        self.assertDictEqual(json.loads(expected_json.decode()), result_json)

    @mock.patch('tropolink.cluster.connect_cluster')
    def test_get_connectivity_dataframe(self, mock_connect_cluster):
        mock_ssh = mock.MagicMock()
        mock_connect_cluster.return_value.__enter__.return_value = mock_ssh
        mock_sftp = mock.MagicMock()
        mock_ssh.open_sftp.return_value = mock_sftp

        with open(os.path.join(os.path.dirname(__file__), 'connectivity.json'), 'rb') as f:
            expected_json = f.read()
            mock_file = io.BytesIO(expected_json)

        mock_sftp.open.return_value.__enter__.return_value = mock_file

        dataframe = get_connectivity_dataframe(ConnectivityFactory())

        self.assertEqual(dataframe["Buffer_1"]["S/A-point_1"], 10)
