import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'accounts.TropolinkUser'

    username = factory.Sequence(lambda n: f'user-{n}')
