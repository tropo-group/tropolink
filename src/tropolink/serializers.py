from rest_framework import serializers


class TemporaryDownloadUrl(serializers.Serializer):
    url = serializers.CharField()
