""" Tropolink API utilility functions connected to the cluster """
import copy
import json
import logging
import os
from contextlib import contextmanager
from datetime import datetime, timedelta
from pathlib import Path
from tempfile import TemporaryFile
from typing import Optional, BinaryIO, List, Dict
from uuid import uuid4

import numpy as np
import pandas as pd
import paramiko
from django.conf import settings
from django.db.models import F, Q
from django.template import loader

from studies.models import Study

logger = logging.getLogger(__name__)


def get_dates(datehour, runtime):
    """
    This function gets the list of dates affected by a period (runtime) starting on a specific date and hour.
    Note that the runtime can be negative.
    :param datehour : starting or arrival date and hour as datetime
    :param runtime : period in hours as float
    :return a tuple containing the number of dates and a dates iterator from the starting date to the ending date
    """

    if runtime > 0:
        start_date = datehour
        end_date = start_date + timedelta(hours=runtime + 3)  # We need to get one more day for 21h
    else:
        end_date = datehour + timedelta(hours=2)  # We need to get one more day for 22h
        start_date = end_date + timedelta(hours=runtime - 2)

    # Create a dates iterator
    dates = pd.date_range(start=start_date, end=end_date, normalize=True)

    return len(dates), dates


def make_control_file(datehour, commons_parameters):
    """
    This function creates a CONTROL file for a specific date and hour.
    It is equivalent to the R function 'CONTROL_make'
    :param datehour: starting or arrival date and hour as datetime
    :param commons_parameters: common hysplit parameters as dictionary
    :return the filename of the created file
    """
    # Get commons parameters
    positions = commons_parameters["positions"]
    nb_positions = len(positions)
    runtime = commons_parameters["runtime"]
    vmotion = commons_parameters["vertical_motion"]
    top_model = commons_parameters["top_of_model"]

    # Get the list of dates
    nb_dates, dates = get_dates(datehour, runtime)

    # Create the meteo filenames
    meteo_files = []
    is_format1 = False
    is_format2 = False
    meteo_dir = ''
    for date in dates:
        if date < datetime.strptime(settings.METEO_CHANGEDATE, "%d/%m/%Y"):
            meteo_files.append(f'{date.strftime("%Y%m%d")}_{settings.METEO_FORMAT1}')
            is_format1 = True
        else:
            meteo_files.append(f'{date.strftime("%Y%m%d")}_{settings.METEO_FORMAT2}')
            is_format2 = True

    if is_format1 and is_format2:
        meteo_dir =  f'{Path(settings.METEO_DIR) / Path(settings.METEO_FORMAT1)} \
                        {Path(settings.METEO_DIR) / Path(settings.METEO_FORMAT2)}/'
    elif is_format1 and not(is_format2):
        meteo_dir = f'{Path(settings.METEO_DIR) / Path(settings.METEO_FORMAT1)}/'
    elif is_format2 and not(is_format1):
        meteo_dir = f'{Path(settings.METEO_DIR) / Path(settings.METEO_FORMAT2)}/'

    # Format the main date
    datestr = datehour.strftime("%y %m %d %H")
    filename_date = datehour.strftime("%y-%m-%d")

    # Create a CONTROL file
    control_filename = f"CONTROL.{filename_date}"
    OUTPUT_PREFIX = settings.OUTPUT_PREFIX
    output_filename = f"{OUTPUT_PREFIX}{filename_date}"

    with open(control_filename, "w") as f:
        f.write(
            loader.render_to_string(
                template_name="control_tpl.txt",
                context={
                    "date": datestr,
                    "nb_locations": str(nb_positions),
                    "locations": positions,
                    "runtime": str(runtime),
                    "vmotion": str(vmotion),
                    "topmodel": str(top_model),
                    "nb_days": str(nb_dates),
                    "meteo_dir": meteo_dir,
                    "meteo_files": meteo_files,
                    "output_dir": os.path.join(".", settings.OUTPUT_DIR),
                    "output_file": output_filename,
                }
            )
        )
    return nb_dates, control_filename


def make_xrun_file(date, study):
    """
    This function creates an xrun file for a specific date.
    :param trajectory: a dictionary containing a trajectory calculation
    :return: the filename of the created file
    """
    # Get the date in the right format
    filename_date = date.strftime("%y-%m-%d")

    # Create a jobname
    study_name = study.dirname

    # Get the paths
    PGM = settings.PGM
    localpath = os.path.join(settings.LOCAL_PATH, study_name)
    outputlist = [localpath, settings.OUTPUT_DIR, settings.OUTPUT_FILE]
    outputpath = os.path.join(*outputlist)
    errorlist = [localpath, settings.OUTPUT_DIR, settings.ERROR_FILE]
    errorpath = os.path.join(*errorlist)

    # Build the filename
    xrun_filename = f"xrun.{filename_date}.scr"

    # Write the xrun file
    with open(xrun_filename, "w") as f:
        f.write(
            loader.render_to_string(
                template_name="xrun_tpl.txt",
                context={
                    "outputpath": outputpath,
                    "errorpath": errorpath,
                    "pgm": PGM,
                    "localpath": localpath,
                    "date": filename_date,
                }
            )
        )

    return xrun_filename


def get_setup_file_content():
    return loader.render_to_string(template_name="SETUP.CFG")


@contextmanager
def connect_cluster():
    """
    Connect to the Cluster
    :return: a context manager to the Cluster
    """
    HOST = settings.TROPOLINK_HOST
    SSH_CONFIG = settings.SSH_CONFIG

    # Go to the tropolink VM
    config = paramiko.SSHConfig()
    config.parse(open(os.path.expanduser(SSH_CONFIG)))
    hcfg = copy.deepcopy(config.lookup(HOST))

    for nonsupportedattr in (
            "serveraliveinterval",
            "identitiesonly",
            "stricthostkeychecking",
            "userknownhostsfile",
            "connecttimeout",
            "hashknownhosts",
    ):
        hcfg.pop(nonsupportedattr, None)

    for attr, cfgattr in (("proxycommand", "sock"), ("user", "username")):
        try:
            val = hcfg.pop(attr, None)
            if val is None:
                continue
            if attr == "proxycommand":
                val = paramiko.ProxyCommand(val)
            hcfg[cfgattr] = val
        except KeyError:
            pass
    keyf = hcfg.pop("identityfile", None)
    if keyf:
        with open(keyf[0]) as keyobj:
            hcfg["pkey"] = paramiko.RSAKey.from_private_key(keyobj)

    with paramiko.SSHClient() as ssh:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(**hcfg)

        yield ssh


def ping_cluster():
    """
    Test the connection to the cluster
    :return the output of an `echo` command
    """
    # Go to the tropolink VM
    with connect_cluster() as ssh:
        msg = "SSH OK"
        ret = ssh.exec_command(f'echo "{msg}"')
        rep = ret[1].read().decode("utf-8").strip()
        assert rep == msg
        return rep


def run_hysplit(study, control_files, xrun_files, dates) -> Dict[str, str]:
    """
    Run an HYSPLIT calculation
    :param trajectory: a dictionary containing a trajectory calculation
    :param control_files: a list of CONTROL files used by HYSPLIT
    :param xrun_files: a list of xrun files used to run HYSPLIT
    """
    LOCAL_PATH = settings.LOCAL_PATH
    HYSPLIT_FILES = settings.HYSPLIT_FILES
    OUTPUT_PREFIX = settings.OUTPUT_PREFIX
    OUTPUT_DIR = settings.OUTPUT_DIR

    # Dict of launched job numbers / date
    jobs = {}

    # Go to the tropolink VM
    with connect_cluster() as ssh:
        sftp = ssh.open_sftp()

        def make_and_change_dir(dirname):
            try:
                sftp.mkdir(dirname)  # Create remote_path
            except IOError:
                pass
            finally:
                sftp.chdir(dirname)

        make_and_change_dir('studies')
        make_and_change_dir(settings.ENVIRONMENT)
        make_and_change_dir(study.user.username)
        make_and_change_dir(study.slug)
        sftp.mkdir('slurmOutput')

        # Put CONTROL and xrun files
        for cfile, xfile in zip(control_files, xrun_files):
            sftp.put(localpath=cfile, remotepath=cfile)
            sftp.put(localpath=xfile, remotepath=xfile)
        setup_content = get_setup_file_content()
        with sftp.open('SETUP.CFG', 'w') as f:
            f.write(setup_content)
        sftp.close()

        # Create the output directory
        output_path = os.path.join(study.dirname, OUTPUT_DIR)
        ssh.exec_command(f"mkdir -p {output_path}")

        # Copy Hysplit files
        for file in HYSPLIT_FILES:
            filepath = os.path.join(LOCAL_PATH, file)
            studypath = os.path.join(LOCAL_PATH, study.dirname)
            ssh.exec_command(f"cp {filepath} {studypath}")

        for xfile, job_date in zip(xrun_files, dates):
            xfilepath = os.path.join(study.dirname, xfile)
            ssh.exec_command(f"chmod +x {xfilepath}")

            # Get the full path
            vmpath = os.path.join(settings.LOCAL_PATH, xfilepath)

            # Run the cluster command
            stdin, stdout, stderr = ssh.exec_command(
                f'module load hysplit && sbatch {vmpath}'
            )
            string = stdout.read().decode("ascii")
            njob = string.split(' ')[-1].strip()
            jobs[njob] = job_date.isoformat()

    return jobs


def update_trajectories_status():
    """
    Update the status of all pending trajectories
    """
    from trajectories.models import Trajectory
    from trajectories.tasks import after_trajectory_success

    pending_trajectories = (
        Trajectory.objects
        .filter(
            Q(jobs__isnull=False, jobs_successful__isnull=True)
            | Q(jobs__isnull=False, jobs__keys__len__gt=F('jobs_successful__len') + F('jobs_failed__len'))
        )
    )

    with connect_cluster() as ssh:
        for trajectory in pending_trajectories:
            slurm_output_directory = Path(trajectory.study.dirname) / 'slurmOutput'

            # Jobs successful
            stdin, stdout, stderr = ssh.exec_command(
                f"cd {slurm_output_directory} && grep -l 'Complete Hysplit' *.out | cut -c 21- | cut -d _ -f 1"
            )
            string = stdout.read().decode()
            trajectory.jobs_successful = [job for job in string.split("\n") if job]

            # Jobs failed
            stdin, stdout, stderr = ssh.exec_command(
                f"cd {slurm_output_directory} && grep -l 'Unable to find file: ' *.out | cut -c 21- | cut -d _ -f 1"
            )
            string = stdout.read().decode()
            jobs_failed = [job for job in string.split("\n") if job]

            stdin, stdout, stderr = ssh.exec_command(
                f"cd {slurm_output_directory} && grep -li 'error' *.err | cut -c 21- | cut -d _ -f 1"
            )
            string = stdout.read().decode()
            jobs_failed += [job for job in string.split("\n") if job]

            trajectory.jobs_failed = jobs_failed
            trajectory.save(update_fields=['jobs_successful', 'jobs_failed'])
            if trajectory.status == "succeeded":
                after_trajectory_success.delay(trajectory.id)


def run_connectivity_computation(connectivity) -> str:
    """
    Run an HYSPLIT calculation
    :param trajectory: a dictionary containing a trajectory calculation
    :param control_files: a list of CONTROL files used by HYSPLIT
    :param xrun_files: a list of xrun files used to run HYSPLIT
    """
    LOCAL_PATH = Path(settings.LOCAL_PATH)
    connectivity_path = LOCAL_PATH / connectivity.dirname
    trajectory_output_path = LOCAL_PATH / connectivity.study.dirname / settings.OUTPUT_DIR

    # Go to the tropolink VM
    with connect_cluster() as ssh:
        sftp = ssh.open_sftp()
        sftp.mkdir(str(connectivity_path))
        sftp.chdir(str(connectivity_path))
        sftp.mkdir('slurmOutput')

        trajectory = connectivity.study.trajectory
        with sftp.open('traj_spec.json', 'w') as f:
            json.dump({
                "name": connectivity.study.slug,
                "creation_date": trajectory.creation_date.isoformat(),
                "hour_utc": trajectory.hour_utc,
                "runtime": trajectory.runtime,
                "vertical_motion": trajectory.vertical_motion,
                "top_of_model": trajectory.top_of_model,
                "dates": list(map(lambda date: date.isoformat(), trajectory.dates)),
                "locations": list(map(lambda location: {"value": location, "valid": True, "id": str(uuid4())}, trajectory.locations)),
            }, f)

        with sftp.open('connectivity_spec.json', 'w') as f:
            json.dump({
                "name": connectivity.study.slug,
                "creation_date": connectivity.creation_date.isoformat(),
                "parameters": {
                    "config": {
                        "method": connectivity.method,
                        "min_nb_hour": 0,
                        "max_nb_hour": int(abs(trajectory.runtime)),
                        "min_altitude": 0,
                        "max_altitude": trajectory.top_of_model,
                        **connectivity.parameters.get('config', {}),
                    },
                    "trajectories_studies": [
                        {
                            "label": connectivity.study.name,
                            "value": trajectory.id
                        }
                    ],
                    "dates": list(map(lambda date: datetime.combine(date, datetime.min.time()).isoformat(), connectivity.dates)),
                    "locations": list(map(lambda location: {"value": location, "valid": True, "id": ""}, connectivity.locations)),
                },
            }, f)

        with sftp.open('connectivity-computation.R', 'w') as f:
            f.write(loader.render_to_string('connectivity-computation.R'))

        with sftp.open('execRunConnectivity.sh', 'w') as f:
            f.write(loader.render_to_string(
                'execRunConnectivity.sh',
                {
                    "connectivity_path": connectivity_path,
                    "tdumpfiles_dir": trajectory_output_path,

                }
            ))

        # Run the cluster command
        script_path = f'{connectivity_path / "execRunConnectivity.sh"}'
        ssh.exec_command(f"chmod +x {script_path}")
        stdin, stdout, stderr = ssh.exec_command(script_path)
        stdout = stdout.read().decode("ascii")
        if stderr is not None:
            stderr = stderr.read().decode("ascii")
        job = stdout.split(' ')[-1].strip()
        if not job:
            logger.error(f'No job number in run_connectivity_computation: {stdout=} {stderr=}')
        return job


def update_connectivities_status():
    """
    Update the status of all pending trajectories
    """
    from connectivities.models import Connectivity

    pending_connectivities = Connectivity.objects.filter(status=Connectivity.Status.IN_PROGRESS)

    with connect_cluster() as ssh:
        for connectivity in pending_connectivities:
            slurm_output_directory = Path(connectivity.dirname) / 'slurmOutput'

            # Job successful
            stdin, stdout, stderr = ssh.exec_command(
                f"cd {slurm_output_directory} && grep -l 'Complete Connectivity' *.out | cut -c 11- | cut -d _ -f 1"
            )
            string = stdout.read().decode()
            if string:
                connectivity.status = Connectivity.Status.SUCCEEDED
                connectivity.progress = None
                connectivity.save(update_fields=['status', 'progress'])
                continue

            # Job failed
            stdin, stdout, stderr = ssh.exec_command(
                f"cd {slurm_output_directory} && grep -l 'Unable to find file: ' *.out | cut -c 11- | cut -d _ -f 1"
            )
            string = stdout.read().decode()
            if string:
                connectivity.status = Connectivity.Status.FAILED
                connectivity.progress = None
                connectivity.save(update_fields=['status', 'progress'])
                continue

            stdin, stdout, stderr = ssh.exec_command(
                f"cd {slurm_output_directory} && grep -l 'Error\|Exécution arrêtée' *.err | cut -c 11- | cut -d _ -f 1"
            )
            string = stdout.read().decode()
            if string:
                connectivity.status = Connectivity.Status.FAILED
                connectivity.progress = None
                connectivity.save(update_fields=['status', 'progress'])
                continue

            # Job in progress
            stdin, stdout, stderr = ssh.exec_command(
                f"cd {slurm_output_directory} && grep 'Percent complete' *.out | tail -n 1 | cut -c 20-"
            )
            string = stdout.read().decode()
            if string:
                try:
                    connectivity.progress = int(string)
                    connectivity.save(update_fields=['progress'])
                except ValueError as e:
                    logger.error(f'Could not parse progress: {e}')
                continue


def clean_tdumps(trajectory):
    """
    Remove tdumps from failed jobs.
    """
    output_path = Path(trajectory.study.dirname) / settings.OUTPUT_DIR
    with connect_cluster() as ssh:
        sftp = ssh.open_sftp()
        try:
            sftp.chdir(str(output_path))
            for job in trajectory.jobs_failed:
                try:
                    sftp.remove(f'tdump_{trajectory.jobs[str(job)][2:]}')
                except Exception as e:
                    logger.debug(f"Could not delete tdump_{trajectory.jobs[str(job)][2:]}: {e}")
        except Exception as e:
            logger.error(f"Error while cleaning tdump files: {e}")
        finally:
            sftp.close()


def get_tdump_list(trajectory) -> List[BinaryIO]:
    output_path = Path(trajectory.study.dirname) / settings.OUTPUT_DIR
    with connect_cluster() as ssh:
        sftp = ssh.open_sftp()
        try:
            sftp.chdir(str(output_path))
            for filename in sftp.listdir():
                if filename.startswith('tdump_'):
                    with sftp.open(filename) as remote_file:
                        yield filename, remote_file
        except Exception as e:
            logger.error(f"Error while getting tdump files: {e}")
        finally:
            sftp.close()


def _get_archive_file(output_path, archive_name, files_pattern='*') -> Optional[BinaryIO]:
    with connect_cluster() as ssh:
        # SFTP Client
        sftp = ssh.open_sftp()

        try:
            # Test if remote_path exists
            sftp.chdir(str(output_path))
            try:
                sftp.stat(archive_name)
            except FileNotFoundError:
                command = f'cd {output_path} && tar czf {archive_name} {files_pattern}'
                stdin, stdout, stderr = ssh.exec_command(command)
                error = stderr.read()
                if error:
                    print(f"Error while creating archive: {error}")
                    logger.error(f"Error while creating archive: {error}")
                    return

            with sftp.open(archive_name) as archive:
                yield archive
        except FileNotFoundError:
            print(f"The study's directory doesn't exist.   {output_path}")
            logger.error("The study's directory doesn't exist.")
            return
        finally:
            sftp.close()


def get_tdumps_archive_file(study: Study) -> Optional[BinaryIO]:
    """
    Create an archive of the tdumps on the cluster and download it as a temporary file
    """
    output_path = Path(study.dirname) / settings.OUTPUT_DIR
    yield from _get_archive_file(output_path, 'tdumps.tar.gz', 'tdump_*')


def get_trajectory_logs_archive_file(study: Study) -> Optional[BinaryIO]:
    """
    Create an archive of the slurm outputs on the cluster and download it as a temporary file
    """
    output_path = Path(study.dirname) / 'slurmOutput'
    yield from _get_archive_file(output_path, 'trajectory_logs.tar.gz')


def get_connectivity_logs_archive_file(connectivity) -> Optional[BinaryIO]:
    output_path = Path(connectivity.dirname) / 'slurmOutput'
    yield from _get_archive_file(output_path, 'connectivity_logs.tar.gz')


def rm_study(study):
    """
    Remove the directory created for this study
    :param study: a study
    """
    study_path = Path(settings.LOCAL_PATH) / study.dirname

    # Go to the tropolink VM
    with connect_cluster() as ssh:
        # Remove the remote study directory
        ssh.exec_command(f"rm -rf {study_path}")


def rm_connectivity(connectivity):
    """
    Remove the directory created for this trajectory calculation
    :param connectivity: a connectivity
    """
    connectivity_path = Path(settings.LOCAL_PATH) / connectivity.dirname

    # Go to the tropolink VM
    with connect_cluster() as ssh:
        ssh.exec_command(f"rm -rf {connectivity_path}")


def get_connectivity_json(connectivity) -> Optional[np.matrix]:
    """
    This function put the connectivity to the tropolink VM as csv file
    :param connectivity: a connectivity object
    :return the file containing the connectivity matrix
    """
    # Remote output path
    connectivity_path = Path(connectivity.dirname)
    filename = f'{connectivity.study.slug}_connectivity_NA.json'

    # Go to the tropolink VM
    with connect_cluster() as ssh:
        # SFTP Client
        sftp = ssh.open_sftp()

        try:
            # Test if remote_path exists
            sftp.chdir(str(connectivity_path))
            with sftp.open(filename) as file:
                return json.load(file)
        except IOError as e:
            print(f"The connectivity result file was not found. {connectivity_path=} {e=}")
            logger.error(f"The connectivity result file was not found. {connectivity_path=} {e=}")
        finally:
            sftp.close()
    return None


def get_connectivity_dataframe(connectivity) -> Optional[pd.DataFrame]:
    """
    This function put the connectivity to the tropolink VM as csv file
    :param connectivity: a connectivity object
    :return the file containing the connectivity matrix
    """
    data = get_connectivity_json(connectivity)
    if data is None:
        return None
    return pd.DataFrame(
        data["connectivity"]["matrix"],
        columns=data["connectivity"]["col_names"],
        index=data["connectivity"]["row_names"],
    )
