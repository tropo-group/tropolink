#!/bin/bash

# Se positionner dans le wd
cd {{ connectivity_path }}

export RscriptName="connectivity-computation.R"
export traj_specs_filename="traj_spec.json"
export connect_specs_filename="connectivity_spec.json"
export tdumpfiles_dir={{ tdumpfiles_dir }}/
export output_dir={{ connectivity_path }}/

export sjobId=./slurmJobId

if [ -f $sjobId ]; then
    rm $sjobId
    touch $sjobId
fi

# chargement des bonnes versions
# voir help avec la commande : module help (sur le frontal)
ml proj/7.0.1 geos/3.9.0 gdal/3.1.0 R/4.0.0

# Go slurm
sbatch <<EOF
#!/bin/sh
#SBATCH --output {{ connectivity_path }}/slurmOutput/connectJob%A_%a.out
#SBATCH --error {{ connectivity_path }}/slurmOutput/connectJob%A_%a.err
#
Rscript $RscriptName $traj_specs_filename $connect_specs_filename $tdumpfiles_dir $output_dir $SLURM_JOB_ID
EOF
