from django.http import Http404, HttpResponse
from django.views import View
from django.views.generic import TemplateView
from rest_framework.response import Response
from rest_framework.views import APIView

import tropolink
from tropolink.cluster import ping_cluster


class HomeView(TemplateView):
    template_name = "home.html"


class VersionView(View):
    def get(self, *args, **kwargs):
        return HttpResponse(f"Version {tropolink.__version__}\n")


class PingView(APIView):
    def get(self, request):
        try:
            return Response({"ret": ping_cluster()})
        except AssertionError:
            raise Http404
