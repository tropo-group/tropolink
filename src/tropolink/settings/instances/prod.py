#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function
import functools
import operator

from ..prod import *

locs_, globs_, env = set_prod_settings(locals())
globals().update(globs_)
# Already set in ansible code, only here for example
# hosts = ['0.0.0.0', 'staging.foo.bar', '.foo.bar',]
# CORS_ORIGIN_WHITELIST = CORS_ORIGIN_WHITELIST + tuple(hosts)
# ALLOWED_HOSTS.extend(hosts)
ALLOWED_HOSTS = ['0.0.0.0', 'tropolink.fr', 'localhost', 'django']
COPS_ALL_HOSTNAMES = ALLOWED_HOSTS + ['0.0.0.0']
CORS_ORIGIN_WHITELIST = functools.reduce(
    operator.iconcat,
    [(f'https://{0}', f'http://{0}') for a in COPS_ALL_HOSTNAMES])
DJANGO__CORS_ORIGIN_ALLOW_ALL = False
SENTRY_DSN = 'https://8d0f51ca717e4dc0ad4d29a795881434@sentry.makina-corpus.net//42'
# vim:set et sts=4 ts=4 tw=80:
