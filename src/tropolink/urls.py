from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.routers import DefaultRouter

from accounts.views import KeycloakTokenView, KeycloakTokenRefreshView
from connectivities.views import ConnectivityViewSet
from studies.views import StudyViewSet
from trajectories.views import TrajectoryViewSet
from tropolink.views import HomeView, PingView, VersionView

router = DefaultRouter()
router.register(r"trajectories", TrajectoryViewSet, basename="trajectories")
router.register(r"studies", StudyViewSet, basename="studies")
router.register(r"connectivities", ConnectivityViewSet, basename="connectivities")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", HomeView.as_view()),
    path("version/", VersionView.as_view()),
    path("ping/", PingView.as_view()),
    path('token/', KeycloakTokenView.as_view()),
    path('token/refresh/', KeycloakTokenRefreshView.as_view()),

    path('docs/schema/', SpectacularAPIView.as_view(patterns=router.urls), name='schema'),
    path('docs/', SpectacularSwaggerView.as_view(url_name='schema'), name='docs'),
] + router.urls


if settings.DEBUG and "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns += [
        path("__debug__/", include(debug_toolbar.urls)),
    ]

admin.site.site_header = "Tropolink"
