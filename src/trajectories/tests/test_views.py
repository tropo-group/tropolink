from unittest import mock

from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

from trajectories.models import Trajectory
from trajectories.tests.factories import TrajectoryFactory, TrajectoryGeometryFactory
from tropolink.tests.factories import UserFactory


class TrajectoryViewTest(TestCase):
    client_class = APIClient

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.trajectory = TrajectoryFactory(
            study__user=cls.user,
            jobs={'12': '42'},
            jobs_successful=['12'],
            jobs_failed=[],
        )
        cls.trajectory_geometry = TrajectoryGeometryFactory(
            trajectory=cls.trajectory
        )

    def setUp(self):
        self.client.force_authenticate(self.user)

    def test_retrieve_trajectory_by_user(self):
        url = reverse('trajectories-detail', kwargs={'pk': self.trajectory.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)

    def test_retrieve_trajectory_by_another_user(self):
        trajectory = TrajectoryFactory()
        url = reverse('trajectories-detail', kwargs={'pk': trajectory.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, response.content)

    @mock.patch('trajectories.views.update_status', mock.MagicMock())
    @mock.patch('trajectories.views.run_hysplit')
    @mock.patch('trajectories.views.compute_distances')
    def test_create(self, mock_compute_distances, mock_run_hysplit):
        url = reverse('trajectories-list')
        response = self.client.post(url, data={
            'name': 'foobar',
            'description': 'foobar',
            'hour_utc': 12,
            'runtime': 25,
            'vertical_motion': 5,
            'top_of_model': 850,
            'dates': ['2022-01-01'],
            'locations': ['1 dot1 44.4104967748132 5.98612657276837 1000'],
        })
        self.assertEqual(response.status_code, 201, response.content)
        self.assertEqual(Trajectory.objects.all().count(), 2)
        mock_run_hysplit.delay.assert_called_once()
        mock_compute_distances.delay.assert_called_once()

    def test_get_tdumps(self):
        url = reverse('trajectories-get-tdumps', kwargs={'pk': self.trajectory.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('url', response.json())

    @mock.patch('trajectories.views.TemporaryTokenAuthentication')
    @mock.patch('trajectories.views.get_tdumps_archive_file')
    def test_get_tdumps_download(self, mock_get_tdumps_archive_file, mock_TemporaryTokenAuthentication):
        mock_TemporaryTokenAuthentication.return_value.authenticate.return_value = (self.user, None)
        url = reverse('trajectories-get-tdumps-download', kwargs={'pk': self.trajectory.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        mock_get_tdumps_archive_file.assert_called_once()

    def test_get_logs(self):
        url = reverse('trajectories-get-logs', kwargs={'pk': self.trajectory.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('url', response.json())

    @mock.patch('trajectories.views.TemporaryTokenAuthentication')
    @mock.patch('trajectories.views.get_trajectory_logs_archive_file')
    def test_get_logs_download(self, mock_get_trajectory_logs_archive_file, mock_TemporaryTokenAuthentication):
        mock_TemporaryTokenAuthentication.return_value.authenticate.return_value = (self.user, None)
        url = reverse('trajectories-get-logs-download', kwargs={'pk': self.trajectory.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        mock_get_trajectory_logs_archive_file.assert_called_once()

    def test_get_geometry(self):
        url = reverse('trajectories-get-geometry', kwargs={'pk': self.trajectory.pk, 'date': '2020-01-01'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertDictEqual(response.json(), self.trajectory_geometry.geometry)

    def test_get_distances_matrix(self):
        url = reverse('trajectories-get-distances-matrix', kwargs={'pk': self.trajectory.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
