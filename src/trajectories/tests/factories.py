import datetime

import factory


class TrajectoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'trajectories.Trajectory'

    study = factory.SubFactory('studies.tests.factories.StudyFactory', trajectory=factory.SelfAttribute('pk'))
    dates = []
    locations = []
    vertical_motion = 0
    top_of_model = 0


class TrajectoryGeometryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'trajectories.TrajectoryGeometry'

    trajectory = factory.SubFactory(TrajectoryFactory)
    date = datetime.date(2020, 1, 1)
    geometry = {'foo': 'bar'}
