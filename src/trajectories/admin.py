from django.contrib import admin
from django.db.models import Q, BooleanField, ExpressionWrapper

from trajectories.models import Trajectory


class TrajectoryInline(admin.TabularInline):
    model = Trajectory
    extra = 0
    can_delete = False
    readonly_fields = ['has_geometries', 'jobs', 'jobs_successful', 'jobs_failed']

    @admin.display(description='Geometries computed', boolean=True)
    def has_geometries(self, instance):
        return instance.has_geometries

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = (
            queryset
            .defer('distances_matrix')
            .annotate(has_geometries=ExpressionWrapper(
                Q(geometries__isnull=False),
                output_field=BooleanField()
            ))
            .distinct()
        )
        return queryset

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False
