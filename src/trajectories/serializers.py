from rest_framework import serializers

from trajectories.models import Trajectory


class TrajectorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Trajectory
        fields = [
            "id",
            "study",
            "creation_date",
            "hour_utc",
            "runtime",
            "vertical_motion",
            "top_of_model",
            "nb_traj_points",
            "dates",
            "locations",
            "name",
            "slug",
            "jobs",
            "jobs_successful",
            "jobs_failed",
            "status",
        ]
        read_only_fields = [
            "id",
            "slug",
            "study",
            "creation_date",
            "nb_traj_points",
            "jobs",
            "jobs_successful",
            "jobs_failed",
        ]

    name = serializers.CharField(source="study.name", read_only=True)
    slug = serializers.CharField(source="study.slug", read_only=True)


class TrajectorySummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Trajectory
        fields = [
            "id",
            "creation_date",
            "hour_utc",
            "name",
            "jobs",
            "jobs_successful",
            "jobs_failed",
            "status",
        ]
        read_only_fields = [
            "id",
            "creation_date",
            "hour_utc",
            "jobs",
            "jobs_successful",
            "jobs_failed",
        ]

    name = serializers.CharField(source="study.name", read_only=True)
