from django.db import transaction
from django.http.response import FileResponse
from django.urls import reverse, get_script_prefix
from drf_spectacular.utils import extend_schema, extend_schema_view, inline_serializer, OpenApiParameter, OpenApiExample
from rest_framework import status, serializers
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.authentication import get_temporary_token, TemporaryTokenAuthentication
from studies.models import Study
from trajectories.models import Trajectory
from trajectories.permissions import IsTrajectoryOwner
from trajectories.serializers import TrajectorySerializer
from trajectories.tasks import run_hysplit, compute_distances, update_status
from tropolink.cluster import get_tdumps_archive_file, get_trajectory_logs_archive_file
from tropolink.serializers import TemporaryDownloadUrl


@extend_schema_view(
    list=extend_schema(
        description='List all the trajectories of the authenticated user.',
    ),
    retrieve=extend_schema(
        description='Get details from a single trajectory.',
    ),
    create=extend_schema(
        description='Create and compute the trajectory and its study.',
    ),
)
class TrajectoryViewSet(
    viewsets.mixins.CreateModelMixin,
    viewsets.mixins.ListModelMixin,
    viewsets.mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    serializer_class = TrajectorySerializer
    permission_classes = [IsAuthenticated, IsTrajectoryOwner]

    def get_queryset(self):
        return (
            Trajectory.objects
            .filter(study__user=self.request.user)
            .select_related('study')
            .defer('distances_matrix')
        )

    def perform_create(self, serializer):
        with transaction.atomic():
            study = Study.objects.create(
                name=self.request.data.get("name"),
                description=self.request.data.get("description"),
                user=self.request.user,
            )

            # Save the trajectory recording
            trajectory = serializer.save(study=study)

        run_hysplit.delay(trajectory.id)
        compute_distances.delay(trajectory.id)

        # Try to update sooner than the cron
        update_status.apply_async(countdown=10)
        update_status.apply_async(countdown=30)
        update_status.apply_async(countdown=60)
        update_status.apply_async(countdown=90)

    @extend_schema(
        description=(
                'Return a url including a unique temporary token to download the tdumps archive.'
                ' The archive is created on demand if it does not exist already, so this can take a while'
                ' on the first hit.'
        ),
        responses=TemporaryDownloadUrl,
    )
    @action(detail=True, url_path='get_tdumps')
    def get_tdumps(self, request, *args, **kwargs):
        token = get_temporary_token(request.user)
        # We remove the prefix because we add it back in the front (so it works locally and in production)
        prefix = get_script_prefix()
        url = reverse('trajectories-get-tdumps-download', args=args, kwargs=kwargs)[len(prefix) - 1:]
        return Response({'url': f'{url}?token={token}'})

    @extend_schema(
        description='Return a file, using a one-time temporary token to authenticate the user.',
        parameters=[
            OpenApiParameter(name='token', location=OpenApiParameter.QUERY),
        ],
        responses={(200, 'application/octet-stream'): bytes},
    )
    @action(detail=True, url_path='get_tdumps_download', authentication_classes=[TemporaryTokenAuthentication])
    def get_tdumps_download(self, request, *args, **kwargs):
        trajectory = self.get_object()
        study = trajectory.study

        if study.trajectory.status != "succeeded":
            return Response(status=status.HTTP_204_NO_CONTENT)

        generator = get_tdumps_archive_file(study)
        try:
            archive_file = next(generator)
        except StopIteration:
            generator.close()
            return Response(status=status.HTTP_204_NO_CONTENT)

        response = FileResponse(
            archive_file,
            content_type='application/octet-stream',
            filename=f'tdumps_{study.slug}.tar.gz',
            as_attachment=True,
        )
        response.block_size = 4048000
        response._resource_closers.append(generator.close)
        return response

    @extend_schema(
        description=(
                'Return a url including a unique temporary token to download the logs archive.'
                ' The archive is created on demand if it does not exist already, so this can take a while'
                ' on the first hit.'
        ),
        responses=TemporaryDownloadUrl,
    )
    @action(detail=True, url_path='get_logs')
    def get_logs(self, request, *args, **kwargs):
        token = get_temporary_token(request.user)
        # We remove the prefix because we add it back in the front (so it works locally and in production)
        prefix = get_script_prefix()
        url = reverse('trajectories-get-logs-download', args=args, kwargs=kwargs)[len(prefix) - 1:]
        return Response({'url': f'{url}?token={token}'})

    @extend_schema(
        description='Return a file, using a one-time temporary token to authenticate the user.',
        parameters=[
            OpenApiParameter(name='token', location=OpenApiParameter.QUERY),
        ],
        responses={(200, 'application/octet-stream'): bytes},
    )
    @action(detail=True, url_path='get_logs_download', authentication_classes=[TemporaryTokenAuthentication])
    def get_logs_download(self, request, *args, **kwargs):
        trajectory = self.get_object()
        study = trajectory.study

        if trajectory.status == 'in_progress':
            return Response(status=status.HTTP_204_NO_CONTENT)

        generator = get_trajectory_logs_archive_file(study)
        try:
            archive_file = next(generator)
        except StopIteration:
            generator.close()
            return Response(status=status.HTTP_204_NO_CONTENT)

        response = FileResponse(
            archive_file,
            content_type='application/octet-stream',
            filename=f'trajectory_logs_{study.slug}.tar.gz',
            as_attachment=True,
        )
        response.block_size = 4048000
        response._resource_closers.append(generator.close)
        return response

    @extend_schema(
        description=(
            'Return the computed geometries as GeoJSON for a single day.'
            ' Include all properties computed in the tdump.'
        ),
        parameters=[
            OpenApiParameter(name='date', location=OpenApiParameter.PATH),
        ],
        examples=[OpenApiExample(
            name='Two locations with two properties computed',
            value={
                "type": "FeatureCollection",
                "features": [
                    {
                        "id": "1",
                        "type": "Feature",
                        "geometry": {
                            "type": "MultiPoint",
                            "coordinates": [
                                [
                                    5.986,
                                    44.41
                                ],
                                [
                                    5.738,
                                    44.051
                                ]
                            ]
                        },
                        "properties": {
                            "AGE": [
                                0.0,
                                -1.0,
                            ],
                            "DAY": [
                                11,
                                10,
                            ]
                        }
                    }
                ]
            }
        )],
        responses=dict,
    )
    @action(detail=True, url_path='get_geometry/(?P<date>[^/.]+)')
    def get_geometry(self, request, date, **kwargs):
        """
            Get geometries of the computed trajectories
        """
        trajectory = self.get_object()
        geometry = trajectory.geometries.filter(date=date).first()
        if geometry is None:
            # The trajectory has not succeeded.
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(geometry.geometry)

    @extend_schema(
        description='Return the computed matrix of distances between each locations.',
        examples=[OpenApiExample(
            name='Two locations',
            value=[
                [
                    0.0,
                    126.44051904366525,
                ],
                [
                    125.88973928439428,
                    0.0,
                ]
            ]
        )],
    )
    @action(detail=True)
    def get_distances_matrix(self, *args, **kwargs):
        return Response(self.get_object().distances_matrix)
