from django.apps import AppConfig


class TrajectoriesConfig(AppConfig):
    name = "trajectories"
