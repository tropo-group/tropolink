import datetime
import gc
import json
import logging
import os
from typing import Union, Literal

from django.contrib.postgres.fields import ArrayField, HStoreField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from trajectories.calculation import compute_geodetic_distances, build_trajectories
from tropolink import cluster

logger = logging.getLogger(__name__)


class TrajectoryGeometry(models.Model):
    trajectory = models.ForeignKey('Trajectory', related_name='geometries', on_delete=models.CASCADE, editable=False)
    date = models.DateField()
    geometry = models.JSONField(editable=False)

    def __str__(self):
        return f'geometry of {self.trajectory} from {self.date.isoformat()}'


class Trajectory(models.Model):
    study = models.OneToOneField("studies.Study", on_delete=models.CASCADE, related_name='trajectory')
    creation_date = models.DateField(auto_now_add=True, editable=False)
    dates = ArrayField(models.DateField())
    locations = ArrayField(models.TextField())
    hour_utc = models.IntegerField(
        default=12,
        validators=[MaxValueValidator(23), MinValueValidator(0)],
    )
    runtime = models.IntegerField(
        default=0,
        validators=[MaxValueValidator(144), MinValueValidator(-144)],
    )
    vertical_motion = models.IntegerField()
    top_of_model = models.IntegerField()
    nb_traj_points = models.IntegerField(null=True, editable=False)
    distances_matrix = models.JSONField(null=True, editable=False)
    jobs = HStoreField(null=True, editable=False)
    jobs_successful = ArrayField(models.IntegerField(), null=True, editable=False)
    jobs_failed = ArrayField(models.IntegerField(), null=True, editable=False)

    class Meta:
        verbose_name_plural = _("trajectories")

    def __str__(self):
        return f'trajectory of {self.study}'

    @cached_property
    def status(self) -> Union[Literal["failed"], Literal["succeeded"], Literal["in_progress"]]:
        try:
            if len(self.jobs) == len(self.jobs_failed):
                return "failed"
            if len(self.jobs) == len(self.jobs_failed) + len(self.jobs_successful):
                return "succeeded"
        except TypeError:
            pass
        return "in_progress"

    def run_hysplit(self):
        # Create the CONTROL and xrun files
        control_files = []
        xrun_files = []
        nb_points = []
        common_parameters = {
            "runtime": self.runtime,
            "vertical_motion": self.vertical_motion,
            "top_of_model": self.top_of_model,
            # Location are of the format "code name lat lon hgt" and we muse only have the lat long hgt
            "positions": [
                ' '.join(location.split(' ')[2:])
                for location in self.locations
            ],
        }

        for date in self.dates:
            # Get the date and hour
            datehour = datetime.datetime.combine(date, datetime.time(hour=self.hour_utc))
            # Create the CONTROL file
            nb_points_date, control_filename = cluster.make_control_file(datehour, common_parameters)
            control_files.append(control_filename)
            nb_points.append(nb_points_date)

            # Create the xrun files
            xrun_files.append(cluster.make_xrun_file(date, self.study))

        self.nb_traj_points = max(nb_points)

        try:
            # Run HYSPLIT
            self.jobs = cluster.run_hysplit(self.study, control_files, xrun_files, self.dates)
            self.save(update_fields=['nb_traj_points', 'jobs'])
        except Exception as e:
            logger.error(f"Error in run_hysplit: {e}")
        finally:
            # Remove the local files
            for cfile, xfile in zip(control_files, xrun_files):
                os.remove(cfile)
                os.remove(xfile)

    def compute_distances(self):
        # Compute geodetic distances from locations coordinates
        distances_mat = compute_geodetic_distances(self.locations)

        self.distances_matrix = distances_mat.tolist()
        self.save(update_fields=['distances_matrix'])

    def compute_geometries(self):
        # Check the trajectory computation success
        if self.status != "succeeded":
            return

        # Get the list of tdump files to use
        for filename, tdump in cluster.get_tdump_list(self):
            try:
                date = filename[6:].split('-')
                date = datetime.date(2000 + int(date[0]), int(date[1]), int(date[2]))
                traj_gdf = build_trajectories(tdump, self.nb_traj_points)
                TrajectoryGeometry.objects.create(
                    trajectory=self,
                    geometry=json.loads(traj_gdf.to_json()),
                    date=date,
                )
                # Free memory, still needed?
                del traj_gdf
                gc.collect()
            except Exception as e:
                logger.error(f'Error while computing geometry from {filename}: {e}')
