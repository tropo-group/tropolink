from typing import BinaryIO

import geopandas as gpd
from itertools import combinations
import numpy as np
from pyproj import Geod
import pandas as pd
from shapely.geometry import MultiPoint


def build_trajectories(tdump: BinaryIO, nb_traj_points):
    """
    This function builds trajectories geometry.
    :param tdump : path to a tdump file
    :param nb_traj_points : the (theorical) number of points by trajectory
    :param with_infos : if False, return a geodataframe containing trajectories as lines.
    If true, return a geodataframe containing trajectories as a list of points with their
    properties (like in a "reshaped" tdump file).
    :return a geodataframe containing the list of trajectories
    """

    # Get the number of rows to skip
    nb_rows_to_skip = 0
    lookup = "PRESSURE"
    for idx, line in enumerate(tdump):
        if lookup in line:
            nb_rows_to_skip = idx
            break

    # Read tdump file
    tdump.seek(0)
    tdump_df = pd.read_csv(
        tdump,
        delim_whitespace=True,
        header=nb_rows_to_skip,
        names=[
            "TRAJ_NB", "MET_GRID_NB", "YEAR", "MONTH", "DAY", "HOUR", "MINUTE", "FORECAST_HOUR",
            "AGE", "LATITUDE", "LONGITUDE", "ALTITUDE", "PRESSURE", "THETA", "AIR_TEMP", "RAINFALL",
            "MIXDEPTH", "RELHUMID", "SPCHUMID", "H2OMIXRA", "TERR_MSL", "SUN_FLUX",
        ],
    )

    # Interpolate missing data
    tdump_df = interpolate_missing_dates(tdump_df, nb_traj_points)

    # Create points in WGS84(epsg:4326)
    tdump_df['LATITUDE'] = tdump_df['LATITUDE'].apply(lambda x: round(x, 3))
    tdump_df['LONGITUDE'] = tdump_df['LONGITUDE'].apply(lambda x: round(x, 3))
    tdump_df = tdump_df.groupby('TRAJ_NB').agg(list)
    geometry = [MultiPoint(list(zip(row["LONGITUDE"], row["LATITUDE"]))) for _, row in tdump_df.iterrows()]
    tdump_gdf = gpd.GeoDataFrame(tdump_df, geometry=geometry, crs={"init": "epsg:4326"})

    return tdump_gdf


def interpolate_missing_dates(tdump_df, nb_traj_points, window=5):
    """
    This functions interpolates values for missing dates in trajectories calculed by Hysplit
    :param tdump_df: pandas dataframe built from a tdump file
    :param nb_traj_points : the (theorical) number of points by trajectory
    :param window : size of the window used to interpolate missing values (default value = 5)
    :return: complete pandas dataframe
    """
    # Group rows by trajectory ids
    gp_points = tdump_df.groupby("TRAJ_NB").count()

    # Identify uncomplete trajectories
    uncomplete_traj = gp_points[gp_points["LONGITUDE"] != nb_traj_points].index

    if uncomplete_traj.shape[0] > 0:
        # For each uncomplete trajectory
        for traj in uncomplete_traj:
            # Get trajectory data
            traj_rows = tdump_df[tdump_df["TRAJ_NB"] == traj]

            # Save absolute ages
            abs_ages = traj_rows["AGE"].abs()

            # Identify missing rows (with AGE column)
            ref_ages = list(range(nb_traj_points))
            new_ages = list(set(ref_ages) - set(abs_ages))

            for new_age in new_ages:
                # Create new rows
                if new_age <= window:
                    slct_rows = traj_rows[
                        (abs_ages > new_age) & (abs_ages <= new_age + window)
                        ]
                    new_row = slct_rows.min()
                else:
                    slct_rows = traj_rows[
                        (abs_ages >= new_age - window) & (abs_ages < new_age)
                        ]
                    new_row = slct_rows.mean()
                # Update the dataframe
                tdump_df = tdump_df.append(new_row, ignore_index=True)

        # Re-format the tdump dataframe
        # if we have backward trajectories
        if tdump_df["AGE"].max() == 0:
            tdump_df.sort_values(
                ["TRAJ_NB", "AGE"], ascending=[True, False], inplace=True
            )
        # if we have forward trajectories
        else:
            tdump_df.sort_values(
                ["TRAJ_NB", "AGE"], ascending=[True, True], inplace=True
            )

        tdump_df.reset_index(inplace=True)

    return tdump_df


def compute_geodetic_distances(locations):
    # Thanks ChatGPT
    # https://forgemia.inra.fr/tropo-group/suivi-de-projet/-/issues/32#note_133270
    locations_df = get_locations_df([location.split(' ') for location in locations])
    latitudes = locations_df['LATITUDE'].to_numpy()
    longitudes = locations_df['LONGITUDE'].to_numpy()
    latitudes = np.radians(latitudes)
    longitudes = np.radians(longitudes)
    dlatitudes = np.subtract.outer(latitudes, latitudes)
    dlongitudes = np.subtract.outer(longitudes, longitudes)
    a = np.sin(dlatitudes / 2) ** 2 + np.cos(latitudes) * np.cos(latitudes.T) * np.sin(dlongitudes / 2) ** 2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    R = 6371  # Earth radius in kilometers
    return c * R


def get_locations_df(locations):
    """
    Build a dataframe with locations infos
    :param locations: list of lines
    :return: a Pandas dataframe
    """
    locations_df = pd.DataFrame(
        locations, columns=["CODE", "NAME", "LATITUDE", "LONGITUDE", "HEIGHT"]
    )
    locations_df[["LONGITUDE", "LATITUDE", "HEIGHT"]] = locations_df[
        ["LONGITUDE", "LATITUDE", "HEIGHT"]
    ].apply(pd.to_numeric)
    return locations_df
