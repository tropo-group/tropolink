from rest_framework import permissions


class IsTrajectoryOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.study.user == request.user
