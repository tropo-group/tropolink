from celery import shared_task
from django.core.cache import cache
from raven.contrib.django.raven_compat.models import client

from trajectories.models import Trajectory
from tropolink.cluster import update_trajectories_status, clean_tdumps


@shared_task
def run_hysplit(trajectory_id):
    trajectory = (
        Trajectory.objects
        .select_related('study')
        .get(id=trajectory_id)
    )
    trajectory.run_hysplit()
    return trajectory_id


@shared_task
def compute_distances(trajectory_id):
    trajectory = (
        Trajectory.objects
        .select_related('study')
        .get(id=trajectory_id)
    )
    trajectory.compute_distances()
    return trajectory_id


@shared_task
def after_trajectory_success(trajectory_id):
    trajectory = (
        Trajectory.objects
        .select_related('study')
        .get(id=trajectory_id)
    )
    # Remove tdumps from failed jobs first so we won't download them in the archive
    clean_tdumps(trajectory)
    # The trajectory is "SUCCEEDED" but geometries are still missing
    # However, it should be computed before a connectivity is computed and should be ok...
    # Actual fix should be to add a status before IN_PROGRESS and SUCCEEDED
    trajectory.compute_geometries()


@shared_task
def update_status():
    # Lock to only have the task run once
    lock_id = 'trajectories_celery_task_update_status'
    locked = cache.add(lock_id, True, 60 * 5)
    if locked:
        try:
            update_trajectories_status()
        except Exception:
            client.captureException()
        cache.delete(lock_id)
