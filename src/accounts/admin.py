from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from accounts.models import TropolinkUser


class TropolinkUserAdmin(UserAdmin):
    
    def get_fieldsets(self, request, obj=None):
        if not obj or request.user.is_superuser:
            return super(TropolinkUserAdmin, self).get_fieldsets(request, obj)
        return (
            (None, {'fields': ('username', 'password')}),
            (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        )

    def has_change_permission(self, request, obj=None):
        if obj is not None and obj.is_superuser:
            return False
        return super(TropolinkUserAdmin, self).has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        if obj is not None and obj.is_superuser:
            return False
        return super(TropolinkUserAdmin, self).has_delete_permission(request, obj)


admin.site.register(TropolinkUser, TropolinkUserAdmin)
