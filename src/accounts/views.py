import json

from keycloak import KeycloakGetError
from keycloak.exceptions import KeycloakAuthenticationError, KeycloakError
from rest_framework.exceptions import AuthenticationFailed, NotAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_keycloak_auth.keycloak import keycloak_openid


class KeycloakTokenView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        username = request.data.get('username', None)
        password = request.data.get('password', None)
        if not username or not password:
            raise NotAuthenticated()
        try:
            tokens_data = keycloak_openid.token(username, password)
        except KeycloakAuthenticationError as e:
            data = json.loads(e.response_body)
            raise AuthenticationFailed({
                'message': data['error_description'],
                'code': data['error'],
            })
        except KeycloakError as e:
            data = json.loads(e.response_body)
            raise AuthenticationFailed({
                'message': data['error_description'],
                'code': data['error'],
            })

        return Response(data=tokens_data)


class KeycloakTokenRefreshView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        refresh_token = request.data.get('refresh', None)
        if not refresh_token:
            raise AuthenticationFailed()
        try:
            tokens_data = keycloak_openid.refresh_token(refresh_token)
        except KeycloakGetError as e:
            raise AuthenticationFailed(e.response_body, e.response_code)
        return Response(data=tokens_data)
