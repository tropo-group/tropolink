from uuid import uuid4

from django.core.cache import cache
from drf_spectacular.authentication import OpenApiAuthenticationExtension
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed

from accounts.models import TropolinkUser


def get_temporary_token(user):
    if user.is_anonymous:
        return None
    token = uuid4()
    cache.set(f'download-{token}', user.id, 300)
    return token


class TemporaryTokenAuthentication(BaseAuthentication):
    def authenticate(self, request):
        token = request.query_params.get('token', None)
        if token is None:
            return None

        user_id = cache.get(f'download-{token}')
        if user_id is None:
            raise AuthenticationFailed('Token has expired')

        try:
            user = TropolinkUser.objects.get(id=user_id)
        except TropolinkUser.DoesNotExist:
            raise AuthenticationFailed('No such user')

        return (user, None)


class TemporaryTokenAuthenticationScheme(OpenApiAuthenticationExtension):
    target_class = TemporaryTokenAuthentication
    name = 'TemporaryTokenAuthentication'

    def get_security_definition(self, auto_schema):
        return {
            'type': 'apiKey',
            'in': 'query',
            'name': 'token',
        }
