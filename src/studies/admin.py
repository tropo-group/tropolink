from django.contrib import admin

from connectivities.admin import ConnectivityInline
from studies.models import Study
from trajectories.admin import TrajectoryInline


class StudyAdmin(admin.ModelAdmin):
    list_display = ("name", "user", "creation_date", "trajectory_status")
    date_hierarchy = "creation_date"
    search_fields = ["name"]
    inlines = [
        TrajectoryInline,
        ConnectivityInline,
    ]
    list_select_related = ['trajectory', 'user']

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.defer('trajectory__distances_matrix')
        return queryset

    def trajectory_status(self, instance):
        if instance.trajectory.jobs is not None:
            if instance.trajectory.jobs_successful is not None:
                return (
                    f'{len(instance.trajectory.jobs_successful)} successful / '
                    f'{len(instance.trajectory.jobs_failed)} failed / '
                    f'{len(instance.trajectory.jobs)} total'
                )
            return f'{len(instance.trajectory.jobs)} total'
        return ''

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


admin.site.register(Study, StudyAdmin)
