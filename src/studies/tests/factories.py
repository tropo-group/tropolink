import factory


class StudyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'studies.Study'

    name = factory.Sequence(lambda n: f'study-{n}')
    user = factory.SubFactory('tropolink.tests.factories.UserFactory')
    trajectory = factory.RelatedFactory('trajectories.tests.factories.TrajectoryFactory', 'study')
