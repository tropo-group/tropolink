from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

from studies.tests.factories import StudyFactory
from tropolink.tests.factories import UserFactory


class TestViews(TestCase):
    client_class = APIClient

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.study = StudyFactory(user=cls.user)

    def setUp(self):
        self.client.force_authenticate(self.user)

    def test_retrieve_study_by_user(self):
        url = reverse('studies-detail', kwargs={'pk': self.study.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)

    def test_retrieve_study_by_another_user(self):
        study = StudyFactory()
        url = reverse('studies-detail', kwargs={'pk': study.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, response.content)

    def test_update_description(self):
        url = reverse('studies-detail', kwargs={'pk': self.study.pk})
        response = self.client.put(url, data={'description': 'foobar'})
        self.assertEqual(response.status_code, 200, response.content)
        self.study.refresh_from_db()
        self.assertEqual(self.study.description, 'foobar')

    def test_update_name(self):
        url = reverse('studies-detail', kwargs={'pk': self.study.pk})
        old_name = self.study.name
        response = self.client.put(url, data={'name': 'foobar'})
        self.assertEqual(response.status_code, 200, response.content)
        self.study.refresh_from_db()
        self.assertEqual(self.study.name, old_name)
