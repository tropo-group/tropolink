from rest_framework import serializers

from connectivities.serializers import ConnectivitySummarySerializer, ConnectivitySerializer
from studies.models import Study
from trajectories.serializers import TrajectorySummarySerializer, TrajectorySerializer


class StudySummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = [
            'id',
            'name',
            'slug',
            'user',
            'creation_date',
            'modification_date',
            'description',
            'trajectory',
            'connectivities',
        ]

    trajectory = TrajectorySummarySerializer()
    connectivities = ConnectivitySummarySerializer(many=True)
    modification_date = serializers.DateField()


class StudySerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = [
            'id',
            'name',
            'slug',
            'user',
            'creation_date',
            'description',
            'trajectory',
            'connectivities',
        ]
        read_only_fields = [
            'name',
            'user',
        ]

    trajectory = TrajectorySerializer(read_only=True)
    connectivities = ConnectivitySerializer(many=True, read_only=True)
