import os

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify


class Study(models.Model):
    name = models.CharField(max_length=30)
    slug = models.CharField(max_length=300, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    creation_date = models.DateField(auto_now_add=True, editable=False)
    description = models.CharField(max_length=280, blank=True, default='')

    class Meta:
        unique_together = [["slug", "user"]]
        verbose_name_plural = _("studies")

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.slug = slugify(self.name)
        return super().save(**kwargs)

    @property
    def dirname(self):
        return os.path.join('studies', settings.ENVIRONMENT, self.user.username, self.slug)
