from django.db.models.signals import post_delete
from django.dispatch import receiver

from studies.models import Study
from tropolink.cluster import rm_study


@receiver(post_delete, sender=Study)
def study_delete_handler(sender, instance, **kwargs):
    rm_study(instance)
