from django.apps import AppConfig


class StudiesConfig(AppConfig):
    name = "studies"

    def ready(self):
        # importing signals
        from . import signals
