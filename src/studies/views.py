from django.db.models import Prefetch, Max
from django.db.models.functions import Greatest
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from connectivities.models import Connectivity
from studies.models import Study
from studies.permissions import IsStudyOwner
from studies.serializers import StudySerializer, StudySummarySerializer


@extend_schema_view(
    list=extend_schema(
        description='List all the studies of the authenticated user.',
        responses=StudySummarySerializer,
    ),
    retrieve=extend_schema(
        description='Get details from a single study.',
        responses=StudySerializer,
    ),
    update=extend_schema(
        description='Update a single study.',
        responses=StudySerializer,
    ),
    destroy=extend_schema(
        description='Remove the study, its trajectory and all of its connectivities.',
    ),
)
class StudyViewSet(mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    serializer_class = StudySerializer
    permission_classes = [IsAuthenticated, IsStudyOwner]

    def get_queryset(self):
        # Detail view
        if 'pk' in self.kwargs:
            return (
                Study.objects
                .filter(user=self.request.user)
                .select_related('trajectory__study')
                .defer('trajectory__distances_matrix')
                .prefetch_related(
                    Prefetch('connectivities', queryset=Connectivity.objects.defer('parameters').order_by('pk'))
                )
            )
        # List view
        return (
            Study.objects
            .filter(user=self.request.user)
            .select_related('trajectory__study')
            .defer('trajectory__distances_matrix')
            .prefetch_related(
                Prefetch('connectivities', queryset=Connectivity.objects.defer('parameters').order_by('pk')),
            )
            .annotate(
                modification_date=Greatest('creation_date', Max('connectivities__creation_date')),
            )
        )

    def get_serializer_class(self):
        # Detail view
        if 'pk' in self.kwargs:
            return StudySerializer
        # List view
        return StudySummarySerializer
