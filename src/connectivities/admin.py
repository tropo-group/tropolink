from django.contrib import admin

from connectivities.models import Connectivity


class ConnectivityInline(admin.TabularInline):
    model = Connectivity
    extra = 0
    can_delete = False
    readonly_fields = ['cluster_job']

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False
