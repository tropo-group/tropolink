import logging
import os

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _

from studies.models import Study
from tropolink.cluster import run_connectivity_computation

logger = logging.getLogger(__file__)


class Connectivity(models.Model):
    class Status(models.TextChoices):
        IN_PROGRESS = "in_progress"
        SUCCEEDED = "succeeded"
        FAILED = "failed"

    study = models.ForeignKey(Study, on_delete=models.CASCADE, related_name='connectivities')
    status = models.CharField(
        max_length=30, choices=Status.choices, default=Status.IN_PROGRESS
    )
    progress = models.IntegerField(null=True, editable=False)
    creation_date = models.DateField(auto_now_add=True, editable=False)
    method = models.CharField(max_length=30)
    dates = ArrayField(models.DateField())
    locations = ArrayField(models.TextField())
    parameters = models.JSONField()
    cluster_job = models.IntegerField(editable=False, null=True)

    class Meta:
        verbose_name_plural = _("connectivities")

    @property
    def name(self) -> str:
        return f'{self.study.name}_{self.id}'

    @property
    def dirname(self):
        return os.path.join(self.study.dirname, f'connectivity_{self.id}')

    def compute(self):
        self.cluster_job = run_connectivity_computation(self)
        if self.cluster_job:
            self.save(update_fields=['cluster_job'])
        else:
            self.status = Connectivity.Status.FAILED
            self.save(update_fields=['status'])
