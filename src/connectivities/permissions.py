from rest_framework import permissions


class IsConnectivityOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.study.user == request.user
