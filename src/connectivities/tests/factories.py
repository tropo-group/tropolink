import factory


class ConnectivityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'connectivities.Connectivity'

    study = factory.SubFactory('studies.tests.factories.StudyFactory')
    method = 'buffer'
    dates = []
    locations = []
    parameters = {}
