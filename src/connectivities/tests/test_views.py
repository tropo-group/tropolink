import datetime
from unittest import mock

import numpy as np
import pandas as pd
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

from connectivities.models import Connectivity
from connectivities.tests.factories import ConnectivityFactory
from tropolink.tests.factories import UserFactory


class TestViews(TestCase):
    client_class = APIClient

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.connectivity = ConnectivityFactory(
            study__user=cls.user,
            status=Connectivity.Status.SUCCEEDED,
            locations=[
                '1 dot1 44.4104967748132 5.98612657276837 1000',
                '2 dot1 44.4104967748132 5.98612657276837 1000',
                '3 dot1 44.4104967748132 5.98612657276837 1000',
            ]
        )

    def setUp(self):
        self.client.force_authenticate(self.user)

    def test_retrieve(self):
        url = reverse('connectivities-detail', kwargs={'pk': self.connectivity.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)

    def test_retrieve_by_another_user(self):
        connectivity = ConnectivityFactory()
        url = reverse('connectivities-detail', kwargs={'pk': connectivity.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, response.content)

    @mock.patch('connectivities.views.compute_connectivity', mock.MagicMock())
    def test_create(self):
        url = reverse('connectivities-list')
        data = {
            'study': str(self.connectivity.study_id),
            'method': 'foobar',
            'dates': ['2021-01-01', '2021-01-02'],
            'locations': ['123', '456'],
            'parameters': {'param1': 1234},
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 201, response.content)
        connectivity = Connectivity.objects.last()
        self.assertEqual(connectivity.method, 'foobar')
        self.assertListEqual(connectivity.dates, [datetime.date(2021, 1, 1), datetime.date(2021, 1, 2)])
        self.assertListEqual(connectivity.locations, ['123', '456'])
        self.assertDictEqual(connectivity.parameters, {'param1': 1234})

    def test_create_empty_dates(self):
        url = reverse('connectivities-list')
        data = {
            'study': str(self.connectivity.study_id),
            'method': 'foobar',
            'dates': [],
            'locations': ['123', '456'],
            'parameters': {'param1': 1234},
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 400, response.content)

    def test_create_empty_locations(self):
        url = reverse('connectivities-list')
        data = {
            'study': str(self.connectivity.study_id),
            'method': 'foobar',
            'dates': ['2021-01-01', '2021-01-02'],
            'locations': [],
            'parameters': {'param1': 1234},
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 400, response.content)

    def test_get_logs(self):
        url = reverse('connectivities-get-logs', kwargs={'pk': self.connectivity.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('url', response.json())

    @mock.patch('connectivities.views.TemporaryTokenAuthentication')
    @mock.patch('connectivities.views.get_connectivity_logs_archive_file')
    def test_get_logs_download(self, mock_get_connectivity_logs_archive_file, mock_TemporaryTokenAuthentication):
        mock_TemporaryTokenAuthentication.return_value.authenticate.return_value = (self.user, None)
        url = reverse('connectivities-get-logs-download', kwargs={'pk': self.connectivity.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        mock_get_connectivity_logs_archive_file.assert_called_once()

    @mock.patch('connectivities.views.get_connectivity_json')
    def test_get_connectivity_matrix(self, mock_get_connectivity_matrix):
        mock_get_connectivity_matrix.return_value = {'foo': 'bar'}
        url = reverse('connectivities-get-connectivity-json', kwargs={'pk': self.connectivity.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        mock_get_connectivity_matrix.assert_called_once()

    @mock.patch('connectivities.views.get_connectivity_dataframe')
    def test_get_connectivity_geojson(self, mock_get_connectivity_dataframe):
        mock_get_connectivity_dataframe.return_value = pd.DataFrame(
            [[0, 1, 2], [3, 4, 5], [6, 7, 8]],
            columns=['col1', 'col2', 'col3'],
            index=['row1', 'row2', 'row3'],
        )
        url = reverse('connectivities-get-connectivity-geojson', kwargs={'pk': self.connectivity.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        mock_get_connectivity_dataframe.assert_called_once()

    @mock.patch('connectivities.views.get_connectivity_dataframe')
    def test_get_connectivity_table(self, mock_get_connectivity_dataframe):
        mock_get_connectivity_dataframe.return_value = pd.DataFrame(
            [[0, 1, 2], [3, 4, 5], [6, 7, 8]],
            columns=['col1', 'col2', 'col3'],
            index=['row1', 'row2', 'row3'],
        )
        url = reverse('connectivities-get-connectivity-table', kwargs={'pk': self.connectivity.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        mock_get_connectivity_dataframe.assert_called_once()
