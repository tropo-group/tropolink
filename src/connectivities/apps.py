from django.apps import AppConfig


class ConnectivitiesConfig(AppConfig):
    name = "connectivities"

    def ready(self):
        # importing signals
        from . import signals