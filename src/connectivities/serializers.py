from rest_framework import serializers

from connectivities.models import Connectivity


class ConnectivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Connectivity
        fields = ["id", "study", "slug", "status", "progress", "creation_date", "method", "dates", "locations", "parameters", "name"]
        read_only_fields = ["id", "creation_date", "status"]

    slug = serializers.CharField(source="study.slug", read_only=True)


class ConnectivitySummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Connectivity
        fields = ["id", "status", "progress", "creation_date", "method", "name"]
        read_only_fields = ["id", "status", "creation_date", "method", "name"]
