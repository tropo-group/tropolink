""" Tropolink API connectivities calculation """

import geopandas as gpd
import pandas as pd
from shapely.geometry import Point

from trajectories.calculation import get_locations_df


def get_connectivity_gdf(connectivity_df, locations):
    # Create a dataframe from the locations list
    locations_df = get_locations_df([location.split(' ') for location in locations])

    # Create points in WGS84(epsg:4326)
    geometry = [
        Point(xy) for xy in zip(locations_df["LONGITUDE"], locations_df["LATITUDE"])
    ]

    connectivity_gdf = gpd.GeoDataFrame(
        connectivity_df, geometry=geometry, crs="epsg:4326"
    )

    return connectivity_gdf
