from django.db.models.signals import post_delete
from django.dispatch import receiver

from connectivities.models import Connectivity
from tropolink.cluster import rm_connectivity


@receiver(post_delete, sender=Connectivity)
def connectivity_delete_handler(sender, instance, **kwargs):
    rm_connectivity(instance)
