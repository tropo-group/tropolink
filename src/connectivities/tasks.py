from celery import shared_task
from django.core.cache import cache
from raven.contrib.django.raven_compat.models import client

from connectivities.models import Connectivity
from tropolink.cluster import update_connectivities_status

@shared_task
def compute_connectivity(connectivity_id):
    connectivity = (
        Connectivity.objects
        .select_related('study')
        .get(id=connectivity_id)
    )
    connectivity.compute()


@shared_task
def update_status():
    # Lock to only have the task run once
    lock_id = 'connectivities_celery_task_update_status'
    locked = cache.add(lock_id, True, 60 * 5)
    if locked:
        try:
            update_connectivities_status()
        except Exception:
            client.captureException()
        cache.delete(lock_id)
