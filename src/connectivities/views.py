import json

from django.http import HttpResponse, FileResponse
from django.urls import reverse, get_script_prefix
from drf_spectacular.utils import extend_schema, extend_schema_view, inline_serializer, OpenApiParameter, OpenApiExample, OpenApiResponse
from rest_framework import status, viewsets, serializers
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.authentication import get_temporary_token, TemporaryTokenAuthentication
from connectivities.calculation import get_connectivity_gdf
from connectivities.models import Connectivity
from connectivities.permissions import IsConnectivityOwner
from connectivities.serializers import ConnectivitySerializer
from connectivities.tasks import compute_connectivity
from tropolink.cluster import get_connectivity_logs_archive_file, get_connectivity_json, get_connectivity_dataframe
from tropolink.serializers import TemporaryDownloadUrl


@extend_schema_view(
    list=extend_schema(
        description='List all the connectivities of the authenticated user.',
    ),
    retrieve=extend_schema(
        description='Get details from a single connectivity.',
    ),
    create=extend_schema(
        description='Create and compute a single connectivity',
    ),
    destroy=extend_schema(
        description='Remove a single connectivity.',
    ),
)
class ConnectivityViewSet(
    viewsets.mixins.CreateModelMixin,
    viewsets.mixins.ListModelMixin,
    viewsets.mixins.RetrieveModelMixin,
    viewsets.mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    serializer_class = ConnectivitySerializer
    permission_classes = [IsAuthenticated, IsConnectivityOwner]

    def perform_create(self, serializer):
        connectivity = serializer.save()
        compute_connectivity.delay(connectivity.id)

    def get_queryset(self):
        return Connectivity.objects.filter(study__user=self.request.user)

    @extend_schema(
        description=(
                'Return a url including a unique temporary token to download the logs archive.'
                ' The archive is created on demand if it does not exist already, so this can take a while'
                ' on the first hit.'
        ),
        responses=TemporaryDownloadUrl,
    )
    @action(detail=True, url_path='get_logs')
    def get_logs(self, request, *args, **kwargs):
        token = get_temporary_token(request.user)
        # We remove the prefix because we add it back in the front (so it works locally and in production)
        prefix = get_script_prefix()
        url = reverse('connectivities-get-logs-download', args=args, kwargs=kwargs)[len(prefix) - 1:]
        return Response({'url': f'{url}?token={token}'})

    @extend_schema(
        description='Return a file, using a one-time temporary token to authenticate the user.',
        parameters=[
            OpenApiParameter(name='token', location=OpenApiParameter.QUERY),
        ],
        responses={(200, 'application/octet-stream'): bytes},
    )
    @action(detail=True, url_path='get_logs_download', authentication_classes=[TemporaryTokenAuthentication])
    def get_logs_download(self, request, *args, **kwargs):
        connectivity = self.get_object()

        if connectivity.status == Connectivity.Status.IN_PROGRESS:
            return Response(status=status.HTTP_204_NO_CONTENT)

        generator = get_connectivity_logs_archive_file(connectivity)
        try:
            archive_file = next(generator)
        except StopIteration:
            generator.close()
            return Response(status=status.HTTP_204_NO_CONTENT)

        response = FileResponse(
            archive_file,
            content_type='application/octet-stream',
            filename=f'connectivity{connectivity.id}_logs_{connectivity.study.slug}.tar.gz',
            as_attachment=True,
        )
        response.block_size = 4048000
        response._resource_closers.append(generator.close)
        return response

    @extend_schema(
        description='Return the connectivity result as JSON.',
        examples=[
            OpenApiExample(
                name='Example',
                value={"name": ["study-name"], "connectivity": {
                    "matrix": [[9, 9],
                               [9, 9]],
                    "row_names": ["S/A-point_3", "S/A-point_4"],
                    "col_names": ["Buffer_3", "Buffer_4"]}, "readme": [
                    "Value at (row i , column j): number of times that trajectories starting/arriving at point i went through buffer j"]}
            )
        ],
        responses={
            200: dict,
        },
    )
    @action(detail=True)
    def get_connectivity_json(self, *args, **kwargs):
        """
            Get the connectivity raw json results
        """
        connectivity = self.get_object()
        data = get_connectivity_json(connectivity)
        return Response(data)

    @extend_schema(
        description='Return the connectivity result as GeoJSON.',
        examples=[
            OpenApiExample(
                name='Example',
                value={"name": ["study-name"], "connectivity": {
                    "matrix": [[9, 9],
                               [9, 9]],
                    "row_names": ["S/A-point_3", "S/A-point_4"],
                    "col_names": ["Buffer_3", "Buffer_4"]}, "readme": [
                    "Value at (row i , column j): number of times that trajectories starting/arriving at point i went through buffer j"]}
            )
        ],
        responses={
            200: dict,
        },
    )
    @action(detail=True)
    def get_connectivity_geojson(self, *args, **kwargs):
        """
            Get the connectivity matrix with the points' geometry
        """
        connectivity = self.get_object()
        connectivity_df = get_connectivity_dataframe(connectivity)
        if connectivity_df is None:
            return Response()

        # Convert to a geodataframe
        connectivity_gdf = get_connectivity_gdf(connectivity_df, connectivity.locations)

        return Response(json.loads(connectivity_gdf.to_json()))

    @extend_schema(
        description='Return the connectivity result as CSV.',
        examples=[
            OpenApiExample(
                name='Example',
                value=",Buffer_3,Buffer_4\nS/A-point_3,9,9\nS/A-point_4,9,9"
            )
        ],
        responses={
            (200, 'text/csv'): OpenApiResponse(response=str, examples=[
                OpenApiExample(
                    name='Example',
                    value=",Buffer_3,Buffer_4\nS/A-point_3,9,9\nS/A-point_4,9,9"
                )
            ]),
        },
    )
    @action(detail=True)
    def get_connectivity_table(self, *args, **kwargs):
        """
            Get the connectivity matrix with the location names
        """
        connectivity = self.get_object()
        connectivity_df = get_connectivity_dataframe(connectivity)
        if connectivity_df is None:
            return Response()

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=connectivity_matrix.csv'  # alter as needed
        connectivity_df.to_csv(path_or_buf=response)

        return response
